package uk.co.tfd.threaded.responses;

import java.util.Arrays;

import uk.co.tfd.Request;
import uk.co.tfd.ResponseHeaders;

/**
 * A 405 response (Method Not allowed).
 * @author ieb
 *
 */
public class HttpMethodNotallowedResponse extends HttpResponse {
	public HttpMethodNotallowedResponse(Request request, String[] allowedMethods) {
		super(request, 405, setupHeaders(request, allowedMethods));
	}

	private static ResponseHeaders setupHeaders(Request request, String[] allowedMethods) {
		ResponseHeaders h = new ResponseHeaders();
		String m = Arrays.toString(allowedMethods);
		h.set("Allow", m.substring(1, m.length()-1));
		h.set("Content-Length", 0);
		return h;
	}


}
