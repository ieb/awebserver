package uk.co.tfd.event;

import uk.co.tfd.Iterables;

/**
 * An empty resoonse containing no EventSources, extend add status.
 * @author ieb
 *
 */
public abstract class EmptyResponse implements Response {


	public final Iterable<EventSource> getEventSources() {
		return Iterables.empty();
	}

}
