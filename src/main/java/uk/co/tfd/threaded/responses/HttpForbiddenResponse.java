package uk.co.tfd.threaded.responses;

import uk.co.tfd.Request;



/**
 * A Forbidden (403) response.
 * @author ieb
 *
 */
public class HttpForbiddenResponse extends  HttpResponse {

	public HttpForbiddenResponse(Request request) {
		super(request, 403);
	}


}
