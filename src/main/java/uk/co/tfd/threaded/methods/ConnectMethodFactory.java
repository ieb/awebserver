package uk.co.tfd.threaded.methods;


/**
 * We are not a proxy at the moment, so dont support connect.
 * @author ieb
 *
 */
public class ConnectMethodFactory extends MethodNotImplementedFactory {
}
