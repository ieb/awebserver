package uk.co.tfd;



/**
 * Creates Methods of a known type.
 * @author ieb
 *
 */
public interface MethodFactory {

	/**
	 * Create a method based on the request
	 * @param request
	 * @param l
	 * @return
	 * @throws HttpRequestProtocolException
	 */
	Method create(Request request) throws HttpRequestProtocolException;

}
