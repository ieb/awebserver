package uk.co.tfd.threaded.methods;

import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Method;
import uk.co.tfd.Request;


public class PostMethodFactory extends MethodNotImplementedFactory {
	public Method create(Request request) throws HttpRequestProtocolException {
		request.checkKeepAlive(true);
		return new PostMethod(request);
	}

}
