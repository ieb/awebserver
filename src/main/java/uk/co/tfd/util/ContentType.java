package uk.co.tfd.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * File to content type converter.
 * @author ieb
 *
 */
public class ContentType {

	private static final Logger LOG = LoggerFactory
			.getLogger(ContentType.class);
	private static final Map<String, String> EXT_TO_MIMETYPE = load(true);
	private static final Map<String, String[]> MIME_TYPE_TO_EXT = load(false);
	private static final String[] EMPTY = new String[0];

	/**
	 * Get the content type of the file.
	 * @param requestedFile the file
	 * @return the content type including charset if appropriate.
	 */
	public static String toString(File requestedFile) {
		String name = requestedFile.getName();
		String[] parts = name.split("\\.");
		String mimeType = EXT_TO_MIMETYPE.get(parts[parts.length - 1]);
		if (mimeType == null) {
			return "application/octet-stream";
		}
		if (mimeType.startsWith("text/")) {
			return mimeType + "; charset=" + detectCharset(requestedFile);
		}
		return mimeType;
	}
	
	/**
	 * get the possible extensions for a mime type
	 * @param mimeType the mime type
	 * @return an array of possible extensions (not including the .)
	 */
	public static String[] getExtensions(String mimeType) {
		String[] exts = MIME_TYPE_TO_EXT.get(mimeType);
		if ( exts == null ) {
			return EMPTY;
		}
		return exts;
	}

	/**
	 * Detect the character set from the file content, currently not implemented.
	 * @param requestedFile
	 * @return
	 */
	private static String detectCharset(File requestedFile) {
		// I could use a charset detection algorithm/library but for the
		// purposes of this code and to save time coding.
		// I will assume that all text is utf-8 encoded.
		return "utf-8";
	}

	@SuppressWarnings("unchecked")
	private static <T> Map<String, T> load(boolean extToMimeType) {
		BufferedReader br = new BufferedReader(new InputStreamReader(
				ContentType.class.getResourceAsStream("mime.types")));
		Map<String, T> m = new HashMap<String, T>();
		try {
			while (true) {
				String line = br.readLine();
				if (line == null)
					break;
				// blank lines
				line = line.trim();
				if (line.length() == 0)
					continue;
				// strip comments
				if (line.charAt(0) == '#')
					continue;
				String[] parts = line.split("#", 2);
				line = parts[0];
				parts = line.split("\\s");
				String mimeType = parts[0].trim();
				if (extToMimeType) {
					for (int i = 1; i < parts.length; i++) {
						String ext = parts[i].trim();
						if (ext.length() > 0) {
							if (m.containsKey(ext)) {
								LOG.warn(
										"Mime type file contains ambiguous extension with 2 or more possible mimetypes, using first Extension:{} is {}  ",
										ext, m.get(ext));
							} else {
								m.put(ext, (T) mimeType);
							}
						}
					}
				} else {
					String[] s = new String[parts.length - 1];
					System.arraycopy(parts, 1, s, 0, s.length);
					m.put(mimeType, (T) s);
				}
			}
		} catch (IOException e) {
			LOG.error("Failed to load mimetype file {} ", e.getMessage(), e);
		} finally {
			try {
				br.close();
			} catch (Exception e) {
				LOG.debug(e.getMessage(), e);
			}
		}
		return m;
	}


}
