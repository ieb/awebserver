package uk.co.tfd;

/**
 * Indicates that reading from the socket timed out either because the client
 * was not sending bytes or the client was sending bytes too slowly.
 * 
 * @author ieb
 * 
 */
public class HttpRequestTimeout extends HttpRequestException {

	public HttpRequestTimeout() {
		super("Timeout waiting for request header bytes");
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7390108929258191080L;

}
