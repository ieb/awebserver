package uk.co.tfd.threaded.views;

import uk.co.tfd.Request;
import uk.co.tfd.threaded.OutputHandler;
import uk.co.tfd.threaded.View;
import uk.co.tfd.threaded.responses.HttpForbiddenResponse;

public class DefaultView implements View {

	private HttpForbiddenResponse outputHandler;

	public DefaultView(Request request) {
		outputHandler = new HttpForbiddenResponse(request);
	}

	public OutputHandler getOutputHander() {
		return outputHandler;
	}

}
