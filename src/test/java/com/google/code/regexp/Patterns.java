package com.google.code.regexp;
/**
 * Taken from http://code.google.com/p/named-regexp/
 * Licensed under ASL2
 * @author clement.denis
 * No maven repo release available.
 * Used while not on Java 7 which has named regex support.
 * 
 * Rewritten as the tests didn't work very well.
 */
import static com.google.common.collect.ImmutableList.of;

import java.util.List;

import com.google.common.collect.ImmutableMap;

public class Patterns {
	

        public final static List<TestingPattern>
                patterns = of(
                		new TestingPattern(
                				"(?<protocol>https?://[^/]+)?/navig/0,\\d*,s(?<siteId>\\d{1,2})_l\\d_d\\d_r(?<rubriqueId>\\d+),00.html(?<anchor>#.*)?",
                				of("protocol", "siteId", "rubriqueId", "anchor"),
                				of(new NamedMatch(
                                        "/navig/0,2158,s31_l1_d0_r3486,00.html",
                    					ImmutableMap.of(
                    							"protocol", NamedMatch.NULL,
                    							"siteId", "31", 
                    							"rubriqueId", "3486", 
                    							"anchor", NamedMatch.NULL)),

                    				new NamedMatch(
                    					"http://www.google.com/navig/0,2158,s31_l1_d0_r3490,00.html#section_test",
                    					ImmutableMap.of(
                    							"protocol", "http://www.google.com",
                    							"siteId", "31",
                    							"rubriqueId", "3490", 
                    							"anchor", "#section_test")),
                    				new NamedMatch(
                                        "https://www.google.fr/navig/0,2599,s31_l1_d0_r3312,00.html",
                                        ImmutableMap.of(
                    							"protocol", "https://www.google.fr",
                    							"siteId", "31", 
                    							"rubriqueId", "3312", 
                    							"anchor", NamedMatch.NULL ))),
                                of("http://www.google.fr/naig/0,2599,s31_l1_d0_r3312,00.html")
                                ),
                        new TestingPattern(
                        		"href=\"javascript\\:openURL\\((?<migrationId>\\d+),(?<type>\\d),'(?<extension>\\w+)'\\)\"",
                        		of("migrationId", "type", "extension"),
                        		of(
                        				new NamedMatch(
                                        "href=\"javascript:openURL(137651,6,'pdf')\"",
                                        ImmutableMap.of(
                    					        "migrationId", "137651",
                    					        "type", "6", 
                    					        "extension", "pdf")),
                                        new NamedMatch(
                                        		"href=\"javascript:openURL(284694,6,'pdf')\"",
                                        		ImmutableMap.of(
	                    							"migrationId", "284694",
	                    							"type", "6",
	                    							"extension", "pdf")),
	                                    new NamedMatch(
                                        		"href=\"javascript:openURL(40414,4,'html')\"",
                                        		ImmutableMap.of(
                            							"migrationId", "40414",
                            							"type", "4",
                            							"extension", "html"))),
                                of("href=\"javascript:openRL(40414,4,'html')\"")
                                ),
                    	new TestingPattern("src=\"(?<url>/download/(?<fileName>\\S*\\.(?<extension>gif|bmp|jpeg|jpg|png)))\"",
                    			of("url", "fileName", "extension"),
                    			of(
                    					new NamedMatch(
                    							 "src=\"/download/0,680270,120835_1,00.gif\"",
                    							 ImmutableMap.of(
                             							"url", "/download/0,680270,120835_1,00.gif",
                             							"fileName", "0,680270,120835_1,00.gif", 
                             							"extension", "gif")),
                    					new NamedMatch(
                    							"src=\"/download/0,,11877_1,00.gif\"",
                    							ImmutableMap.of(
                            							"url", "/download/0,,11877_1,00.gif",
                            							"fileName", "0,,11877_1,00.gif",
                            							"extension", "gif")),
                            			new NamedMatch(
                            					"src=\"/download/0,671975,107350_1,00.jpg\"",
                            					ImmutableMap.of(
                            							"url", "/download/0,671975,107350_1,00.jpg",
                            							"fileName", "0,671975,107350_1,00.jpg",
                            							"extension", "jpg"))),
                                of("src=\"/downlod/0,680270,120835_1,00.gif\"")
                                )
                          );
                            						


        public static final List<String>
                standardPatterns = of(
                        "(https?://[^/]+)?/navig/0,\\d*,s(\\d{1,2})_l\\d_d\\d_r(\\d+),00.html(#.*)?",
                        "href=\"javascript\\:openURL\\((\\d+),(\\d),'(\\w+)'\\)\"",
                        "src=\"(/download/(\\S*\\.(gif|bmp|jpeg|jpg|png)))\"");


}