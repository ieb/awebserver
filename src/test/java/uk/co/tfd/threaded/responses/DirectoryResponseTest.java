package uk.co.tfd.threaded.responses;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.Request;
import uk.co.tfd.event.responses.DummyRequest;
import uk.co.tfd.threaded.RequestHandler;
import uk.co.tfd.threaded.ThreadedRequest;
import uk.co.tfd.threaded.views.DirectoryResponse;
import uk.co.tfd.util.HttpDate;
import uk.co.tfd.util.ParsedRequestLine;

public class DirectoryResponseTest extends AbsrtractResponseTest {

	private static final Logger LOG = LoggerFactory
			.getLogger(DirectoryResponseTest.class);

	@Test
	public void test() throws IOException, HttpRequestException {

		Request request = new DummyRequest();
		DirectoryResponse fbr = new DirectoryResponse(request, new File(
				"src/test/java/uk/co/tfd/threaded/responses"));
		String result = responseTest(fbr);
		LOG.info("Response is {} ", result);
		checkResponseString(result, 200);
		// we just checked for CRNL endings so this is ok.
		final BufferedReader br = new BufferedReader(new StringReader(result));
		RequestHandler requestHandler = new RequestHandler() {

			public void writeByte(byte[] buffer, int off, int len)
					throws IOException {
				Assert.fail("Not exepecting a write during parsing headers");
			}

			public String readLine(int spaceBefore) throws IOException,
					HttpRequestException {
				return br.readLine();
			}

			public int readByte(byte[] buffer, int off, int len)
					throws IOException {
				Assert.fail("Not exepecting a bulk read during parsing headers");
				return -1;
			}

			public void flush() throws IOException {
			}
		};
		// present its a GET operation to see if we can parse the headers.
		ParsedRequestLine dummy = new ParsedRequestLine(
				"GET /testing/location HTTP/1.1");
		ThreadedRequest t = new ThreadedRequest(requestHandler, dummy, 1000,
				100);
		Map<String, List<String>> requestHeaders = t.getHeaders();
		Assert.assertTrue(requestHeaders.containsKey("Date"));
		Assert.assertTrue(requestHeaders.containsKey("Content-Length"));
		Assert.assertTrue(requestHeaders.containsKey("Last-Modified"));
		Assert.assertTrue(requestHeaders.containsKey("Content-Type"));
		Assert.assertTrue(requestHeaders.containsKey("Server"));
		Assert.assertArrayEquals(new String[] { "text/html; charset=utf-8" },
				requestHeaders.get("Content-Type").toArray());
		Date serverDate = HttpDate.parse(requestHeaders.get("Date").get(0));
		Date lastModifiedDate = HttpDate.parse(requestHeaders.get(
				"Last-Modified").get(0));
		Assert.assertTrue(lastModifiedDate.before(serverDate));
		Assert.assertTrue(serverDate.before(new Date()));

	}

}
