package uk.co.tfd.threaded.responses;

import uk.co.tfd.ResponseHeaders;
import uk.co.tfd.Request;

/**
 * A permanent redirect 301, not to be confused with temp redirect or a form
 * post redirect.
 * 
 * @author ieb
 * 
 */
public class HttpPermRedirectResponse extends HttpResponse {
	public HttpPermRedirectResponse(Request request, String location) {
		super(request, 301, setLocation(request, location));
	}

	private static ResponseHeaders setLocation(Request request, String location) {
		ResponseHeaders h = new ResponseHeaders();
		h.set("Location", location);
		return h;
	}

}
