package uk.co.tfd.threaded.methods;


/**
 * Not supporting deletes from clients.
 * @author ieb
 *
 */
public class DeleteMethodFactory extends MethodNotImplementedFactory {
}
