package uk.co.tfd.threaded;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpClientClosedStream;
import uk.co.tfd.HttpRequestTimeout;
import uk.co.tfd.HttpRequestException;
import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.MethodFactory;
import uk.co.tfd.MethodRegistry;
import uk.co.tfd.threaded.responses.HttpResponse;
import uk.co.tfd.util.ParsedRequestLine;

/**
 * This handles the client socket. Reading requests, parsing headers, creating
 * methods and passing off to those methods for the response. It exists as a
 * runnable because the threaded dispatcher creates these objects and places
 * them on a queue for execution.
 * 
 * @author ieb
 * 
 */
public class SocketHandler implements Runnable, RequestHandler,
		Comparable<SocketHandler> {

	private static final Logger LOG = LoggerFactory
			.getLogger(SocketHandler.class);
	/**
	 * The client socket.
	 */
	private Socket socket;
	/**
	 * Socket input stream.
	 */
	private InputStream in;
	/**
	 * Socket output stream.
	 */
	private OutputStream out;
	/**
	 * Registry of MethodFactories which generate Methods on demand.
	 */
	private MethodRegistry methodRegistry;
	/**
	 * A buffer used for reading requests. Assumes that no line will be > 2K in
	 * length..
	 */
	private byte[] buffer = new byte[2048];
	/**
	 * How long to keep the socket open for before the server closes it.
	 */
	private long keepAliveTTL;
	/**
	 * The current request.
	 */
	private ThreadedRequest request;
	/**
	 * The number of requests this socket handler has processed so far.
	 */
	private int nrequests;
	/**
	 * The ms time when this item was placed on the execution queue.
	 */
	private long qtime;

	/**
	 * Create a socket handler for a socket. This object once created should be
	 * queued for execution.
	 * 
	 * @param s
	 *            the socket
	 * @param methodRegistry
	 *            a registry containing MethodFactories that can create Methods
	 *            on demand.
	 * @param keepAliveTTL
	 *            how long in ms to persist the connection.
	 * @param maxRequests
	 *            the maximum number of request the socket handler should
	 *            process before the connection is terminated.
	 * @throws IOException
	 *             in anything goes wrong reading or writing to the socket.
	 *             Other IOExceptions should be reported via the http protocol.
	 */
	public SocketHandler(Socket s, MethodRegistry methodRegistry,
			long keepAliveTTL, int maxRequests) throws IOException {
		LOG.debug("Adding Socket Handler {} far end {} ", this, s.getPort());
		this.socket = s;
		this.in = socket.getInputStream();
		this.out = socket.getOutputStream();
		this.methodRegistry = methodRegistry;
		this.keepAliveTTL = keepAliveTTL;
		this.nrequests = maxRequests;
		qtime = System.currentTimeMillis();
	}

	/** 
	 * Handles reading the socket with a single thread. This will be called by the threaded queue executor using a thread from
	 * its thread pool.
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		LOG.debug(
				"Starting Thread for socket handler for remote port {} was in queue for {}ms ",
				new Object[] { socket.getPort(),
						System.currentTimeMillis() - qtime });
		boolean keepAlive = true;
		long endKeepAlive = System.currentTimeMillis() + keepAliveTTL;
		while (keepAlive) {
			try {
				try {
					keepAlive = processRequest(endKeepAlive);
					out.flush();
				} catch (HttpRequestProtocolException e) {
					// deal with protocol errors informing the client and
					// closing the stream.
					// Although we could attempt to keep alive the errors
					// may be due to a mallicious client so
					// we might as well not give them a second chance to tie
					// up the server thread.
					new HttpResponse(e.getCode()).output(this);
					keepAlive = false;
				} catch (HttpRequestTimeout e) {
					LOG.info("Closing Idle connection");
					keepAlive = false;
				} catch (HttpRequestException e) {
					LOG.debug("Response Exception, closing socket {} ",
							e.getMessage(), e);
					LOG.error("Response Exception, closing socket {} ",
							e.getMessage());
					keepAlive = false;
				}
			} catch (IOException e) {
				// Only IO execeptions that prevent us from sending to the
				// client should arrive here.
				if (e instanceof SocketException
						&& e.getMessage().startsWith("Broken pipe")) {
					LOG.error("Failed to process request {} ", e.getMessage(),
							e);
				} else {
					LOG.error("Failed to process request {} ", e.getMessage(),
							e);
				}
				keepAlive = false;
			}
		}
		try {
			socket.close();
		} catch (Exception e) {
			LOG.debug("Failed to close socket. {} ", e.getMessage(), e);
		}
	}

	/**
	 * Process the request.
	 * @param endKeepAlive time after which the connection must be terminated.
	 * @return true if the connection can persist.
	 * @throws IOException
	 * @throws HttpRequestException
	 */
	boolean processRequest(long endKeepAlive) throws IOException,
			HttpRequestException {
		ThreadedMethod m = checkRequest(endKeepAlive);
		return request.done(m.processRequest(this, endKeepAlive));
	}

	/**
	 * Check the request line with a timeout. If the timeout is reached while reading the request line
	 * the request will be aborted.
	 * @param endKeepAlive the time (epoch ms) when the request must have been read.
	 * @return the method represented by the request line with headers already parsed, but the body not read.
	 * @throws IOException
	 * @throws HttpRequestException
	 */
	private ThreadedMethod checkRequest(long endKeepAlive) throws IOException,
			HttpRequestException {
		ParsedRequestLine parsedRequestLine = new ParsedRequestLine(readLine(
				methodRegistry.getMaxMethodLength(), endKeepAlive));
		MethodFactory mf = methodRegistry.get(parsedRequestLine.getMethod());
		if (mf != null) {
			request = new ThreadedRequest(this, parsedRequestLine,
					endKeepAlive, nrequests);
			nrequests--;

			return (ThreadedMethod) mf.create(request);
		}
		throw new HttpRequestProtocolException(501); // spec 5.1.1
	}

	/**
	 * Read a line with no limitations on the number of bytes that can 
	 * be read before a space must be encountered.
	 * 
	 * @see uk.co.tfd.threaded.RequestHandler#readLine(int)
	 */
	public String readLine(int spaceBefore) throws IOException,
			HttpRequestException {
		return readLine(spaceBefore, -1);
	}

	/**
	 * Read a line with timeout. If the current ms time exceeds waitUntil on the
	 * first char, and wait until is not 0, then the method will abort.
	 * 
	 * @return a line from the client upto but not including CRLF
	 * @throws IOException
	 *             when the client socket can't be read from.
	 * @throws HttpRequestException
	 *             if there is a problem with the request.
	 */
	private String readLine(int spaceBefore, long waitUntil)
			throws IOException, HttpRequestException {
		int s = 0;
		int i = 0;
		boolean noSpace = true;
		for (; s != 2 && i < buffer.length; i++) {
			if (waitUntil > 0) {
				while (in.available() == 0) {
					if (System.currentTimeMillis() > waitUntil) {
						throw new HttpRequestTimeout();
					}
					Thread.yield();
				}
			}
			int c = in.read();
			if (spaceBefore > 0 && noSpace && i > spaceBefore) {
				throw new HttpRequestProtocolException(501); // spec 5.1.1
			}
			if (c == ' ') {
				noSpace = false;
			}
			if (c < 0) {
				if (i == 0) {
					throw new HttpClientClosedStream("Client closed stream");
				}
				throw new IOException("Unexpected end of stream");
			}
			buffer[i] = (byte) c;
			switch (s) {
			case 0:
				if (c == '\r')
					s = 1;
				break;
			case 1:
				if (c == '\n') {
					s = 2;
				} else {
					s = 0;
				}
				break;
			}
			// reset the wait until for slow senders, we will close at < 1 byte
			// per second during the headers.
			waitUntil = System.currentTimeMillis() + 1000;
		}
		if (s == 2) {
			// Headers should be ASCII as per rfc822 section 3.1
			String line = new String(buffer, 0, i - 2);
			return line;
		} else {
			throw new HttpRequestProtocolException(414);
		}
	}

	/** 
	 * Reads bytes from the body into a buffer taking account of the size of
	 * the body if it was specified by a content length header. When the end of the 
	 * body or the end of the stream is reached a -1 is returned.
	 * @see uk.co.tfd.threaded.RequestHandler#readByte(byte[], int, int)
	 */
	public int readByte(byte[] buffer, int off, int len) throws IOException {
		// honor content length in the request.
		return request.adjustBodyRead(in.read(buffer, off,
				request.getBytesToRead(len)));
	}

	/**
	 * Write bytes out
	 * @see uk.co.tfd.threaded.RequestHandler#writeByte(byte[], int, int)
	 */
	public void writeByte(byte[] buffer, int off, int len) throws IOException {
		out.write(buffer, off, len);
	}

	public int compareTo(SocketHandler o) {
		// TODO: Implement priority handling of requests.
		return 0;
	}

	public void flush() throws IOException {
		out.flush();		
	}
}
