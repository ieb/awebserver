package uk.co.tfd.event;


/**
 * Event Sources are used by handlers to encapsulate non blocking operations.
 * EventSources are returned inside a Response to the main event dispatcher, and
 * registered with the selector so that when non blocking IO completes the
 * source can notify the a thread of the event and the EventHandler registered
 * to the event can handle the event. EventSources must conform to standard
 * events that a NIO Selector can handle.
 * 
 * We could have extended a SlectableChannel however, that would have made it harder for 
 * the implementor to attach an EventHandler.
 * 
 * @author ieb
 * 
 */
public interface EventSource {

	/**
	 * Register this event source with a selector and attach a suitable
	 * EventHandler to the registration. An event handler must be registered so
	 * that the event loop has somewhere to deliver the buffer to.
	 * 
	 * @param eventDispatcher
	 */
	void register(EventDispatcher eventDispatcher);

}
