package uk.co.tfd.event;

import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;

/**
 * Although an event dispatcher does quite a bit the only method really required
 * is to allow things to register selectable channels with the central selector used 
 * by or more event loops.
 * 
 * @author ieb
 * 
 */
public interface EventDispatcher {

	/**
	 * Register a selectable channel the selector in the event dispatcher.
	 * @param selectabelChannel
	 * @param ops
	 * @param attachment
	 * @return
	 * @throws ClosedChannelException
	 */
	SelectionKey register(SelectableChannel selectabelChannel, int ops,
			Object attachment) throws ClosedChannelException;

}
