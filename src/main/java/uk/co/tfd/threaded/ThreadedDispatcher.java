package uk.co.tfd.threaded;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.MethodRegistry;

/**
 * Opens a server socket, accepts client connections and creates SocketHandler
 * objects with each accepted connection. Those SocketHandler objects are placed
 * on a concurrent non blocking priority queue. A ThreadPoolExecutor takes items
 * from the queue and executes their run method.
 * 
 * @author ieb
 * 
 */
public class ThreadedDispatcher {

	private static final Logger LOG = LoggerFactory
			.getLogger(ThreadedDispatcher.class);
	/**
	 * true when running.
	 */
	private boolean running;
	/**
	 * the port which this threaded dispatcher will accept connections on.
	 */
	private int port;
	/**
	 * The maximum number of threads.
	 */
	private int threads;
	/**
	 * The maximum amount of time connections should be persistent (kept alive).
	 */
	private long keepAliveTTL;
	/**
	 * The maximum number of a request a connection can process before it is terminated.
	 */
	private int maxRequests;

	/**
	 * @param threads
	 *            the maximum number of threads in the thread pool
	 * @param port
	 *            the port number
	 * @param keepAliveTTL
	 *            the number of ms that a connection may be kept open.
	 * @param maxRequests
	 *            the maximum number of requests a connection may serve
	 */
	public ThreadedDispatcher(int threads, int port, long keepAliveTTL,
			int maxRequests) {
		this.port = port;
		this.threads = threads;
		this.maxRequests = maxRequests;
		this.keepAliveTTL = keepAliveTTL;
	}

	/**
	 * Start the main loop that creates the server socket and than accepts
	 * connection request. Once a request is accepted. it will be placed on a
	 * queue, and processed by a ThreadPoolExecutor.
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void start() throws IOException, InterruptedException {
		LOG.info(
				"Starting Server on port {} with {} threads, Keep Alive connections for {} ms or {} requests ",
				new Object[] { this.port, this.threads, this.keepAliveTTL,
						this.maxRequests });
		MethodRegistry methodFactories = MethodRegistryLoader.getFactories();
		// It would be nice to allow priority, however there is hardly any information available when the socket is accepted 
		// to set the priority, other than remote address. We could give administrators priority by subnet range.
		BlockingQueue<Runnable> workQueue = new PriorityBlockingQueue<Runnable>();
		ThreadPoolExecutor pool = new ThreadPoolExecutor(threads / 2, threads,
				120, TimeUnit.SECONDS, workQueue);
		ServerSocket ss = new ServerSocket(port, 100);
		running = true;
		while (running) {
			Socket s = ss.accept();
			pool.execute(new SocketHandler(s, methodFactories, keepAliveTTL,
					maxRequests));
		}
		pool.awaitTermination(20, TimeUnit.SECONDS);
		LOG.info("Started Finished on port {} ", this.port);
	}

	/**
	 * Just set the stop flag and let the server shout down gracefully.
	 */
	public void stop() {
		running = false;

	}

}
