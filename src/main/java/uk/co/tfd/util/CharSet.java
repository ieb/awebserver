package uk.co.tfd.util;

import java.nio.charset.Charset;

/**
 * Utility charset class.
 * @author ieb
 *
 */
public class CharSet {
	public static final Charset UTF8 = Charset.forName("UTF-8");
}
