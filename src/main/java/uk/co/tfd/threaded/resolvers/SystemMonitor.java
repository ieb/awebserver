package uk.co.tfd.threaded.resolvers;

import java.util.Timer;
import java.util.TimerTask;

public class SystemMonitor {

	private static final Timer SYSTEM_MONITOR_TIMER = new Timer(true);

	public static void schedule(TimerTask timerTask, long delay) {
		SYSTEM_MONITOR_TIMER.schedule(timerTask,delay, delay);
	}

}
