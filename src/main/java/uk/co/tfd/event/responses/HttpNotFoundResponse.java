package uk.co.tfd.event.responses;

import uk.co.tfd.Request;

public class HttpNotFoundResponse extends HttpResponse {
	
	public HttpNotFoundResponse(Request request) {
		super(404);
	}

}
