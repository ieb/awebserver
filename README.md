In this source tree are is a Java web server and some tests.

In the java source code there are 2 versions of the web server.

A standard threaded web server that uses a accept loop and dispatches work to a concurrent worker queue. 
Those workers are executed by a dedicated thread that will if the client is HTTP 1.0, serve a single request. 
The request object is bound to the thread while the thread is being processed.
If the client is HTTP 1.1 the thread will serve upto 100 requests or for 5 seconds, whichever comes sooner.

The thread pool contains 50 threads and is allowed to grow to 100.
The server supports GET and POST operations. With GETs coming from the file system where there is come content.

The second server is incomplete but is based on a event loop thread, accepting events from a NIO selector and dispatching the work to 
event processing threads. Request objects are bound to the sockets which they are associated and maintain state. The dispatcher in this
case works on the request object when there is work to do as a result of reading content from the socket, or as a result of the socket 
becoming available to write to. Request objects may respond with additional non blocking channels that can be registered with the event
loop to allow the transfer of bytes from slow sources to the out bound sockets. The intention was that this server should be run with 
1 thread per core serving 1000s of sockets.

to build

mvn clean install

to run

java exec:java

or

java -jar target/webserver-1.0-SNAPSHOT.jar 

then point your browser to http://localhost:9000



