package uk.co.tfd.threaded;

import java.io.IOException;

/**
 * Handles output, able to write that output to a RequestHandler.
 * 
 * @author ieb
 * 
 */
public interface OutputHandler {

	/**
	 * Output content contained within the implementation of the OutputHandler
	 * to a request handler.
	 * 
	 * @param requestHandler
	 * @throws IOException
	 */
	void output(RequestHandler requestHandler) throws IOException;

	/**
	 * @return the status code of the response, should chain down to embedded
	 *         handlers are required to locate the status code.
	 */
	int getStatusCode();

}
