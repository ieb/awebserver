package com.google.code.regexp;

import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

import junit.framework.Assert;

public class NamedMatch {
	public static final String NULL = "---null---";

	private static final Logger LOG = LoggerFactory.getLogger(NamedMatch.class);

	private String testPattern;
	private Map<String, String> groupMatches;
	private NamedMatcher namedMatcher;

	public NamedMatch(String testPattern, Map<String, String> groupMatches) {
		this.testPattern = testPattern;
		this.groupMatches = groupMatches;
	}
	
	public void check(NamedPattern p) {
		namedMatcher = p.matcher(testPattern);
		Assert.assertTrue(namedMatcher.matches());
		// the intersection of the maps are the same as the maps, and no differences.
		Map<String, String> matchedGroups = cleanMatchedGroups(namedMatcher.namedGroups());
		LOG.info("Expected {} Got {} ",groupMatches, matchedGroups);
		Assert.assertEquals(groupMatches.size(), matchedGroups.size());
		Assert.assertEquals(groupMatches.size(), Sets.intersection(groupMatches.entrySet(), matchedGroups.entrySet()).size());
		Assert.assertEquals(0,Sets.difference(groupMatches.entrySet(), matchedGroups.entrySet()).size());
	}

	/**
	 * Sets null values to NULL to match the Immutable maps that cant contain null values
	 * @param namedGroups
	 * @return
	 */
	private Map<String, String> cleanMatchedGroups(
			Map<String, String> namedGroups) {
		for ( Entry<String, String> e : namedGroups.entrySet()) {
			if ( e.getValue() == null ) {
				e.setValue(NULL);
			}
		}
		return namedGroups;
	}
	

}
