package uk.co.tfd.event;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Iterator;

import uk.co.tfd.MethodRegistry;

public class EventDispatcherWorker implements Runnable, EventDispatcher {

	private boolean running;
	private MethodRegistry methodRegistry;
	private ByteBuffer buffer;
	private Selector selector;

	public EventDispatcherWorker(Selector selector) {
		this.selector = selector;
		this.buffer = ByteBuffer.allocateDirect(4096);
		this.methodRegistry = MethodRegistryLoader.getRegistry();
	}

	public void run() {
		try {
			running = true;
			while (running) {
				selector.select();
				Iterator<SelectionKey> ikey = selector.selectedKeys()
						.iterator();
				while (ikey.hasNext()) {
					dispatchEvent(this, ikey, ikey.next());
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			running = false;
		}
	}

	public SelectionKey register(SelectableChannel channel, int ops,
			Object attachment) throws ClosedChannelException {
		return channel.register(selector, ops, attachment);
	}

	protected void dispatchEvent(EventDispatcher eventDispatcher,
			Iterator<SelectionKey> ikey, SelectionKey key) {
		try {
			ikey.remove(); // indicate its being processed.
			if (key.isAcceptable()) {
				SocketChannel acceptedChannel = ((ServerSocketChannel) key
						.channel()).accept();
				eventDispatcher.register(acceptedChannel, SelectionKey.OP_READ
						| SelectionKey.OP_WRITE, new RequestHandler(
						methodRegistry, 4096));
			} else if (key.isConnectable()) {
			} else if (key.isReadable()) {
				ReadableByteChannel channel = (ReadableByteChannel) key
						.channel();
				try {
					InputHander rh = (InputHander) key.attachment();
					channel.read(buffer);
					buffer.flip();
					Response r = rh.update(buffer);
					for (EventSource es : r.getEventSources()) {
						es.register(eventDispatcher);
					}
					if (r.end()) {
						channel.close();
					}
				} catch (Exception e) {
					channel.close();
				}
			} else if (key.isWritable()) {
				WritableByteChannel channel = (WritableByteChannel) key
						.channel();
				try {
					OutputHandler rh = (OutputHandler) key.attachment();
					Response r = rh.fillOutputBuffer(buffer);
					buffer.flip();
					channel.write(buffer);
					for (EventSource es : r.getEventSources()) {
						es.register(eventDispatcher);
					}
					if (r.end()) {
						channel.close();
					}
				} catch (Exception e) {
					channel.close();
				}
			}
		} catch (IOException e) {

		}

	}

	public void stop() {
		running = false;
	}

}
