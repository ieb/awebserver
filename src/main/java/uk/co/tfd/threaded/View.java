package uk.co.tfd.threaded;

/**
 * Encapsulates a resolved view.
 * @author ieb
 *
 */
public interface View {

	/**
	 * @return the output handler for the view.
	 */
	OutputHandler getOutputHander();

}
