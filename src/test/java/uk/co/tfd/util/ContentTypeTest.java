package uk.co.tfd.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import junit.framework.Assert;

import org.junit.Test;

public class ContentTypeTest {

	@Test
	public void test() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(
				ContentTypeTest.class
						.getResourceAsStream("contenttypetest.txt")));
		while(true) {
			String line = br.readLine();
			if ( line == null ) break;
			String[] parts = line.split(" ",2);
			Assert.assertEquals(parts[1], ContentType.toString(new File(parts[0])));
		}
		br.close();
	}

}
