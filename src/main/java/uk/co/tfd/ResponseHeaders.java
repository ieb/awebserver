package uk.co.tfd;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import uk.co.tfd.util.HttpDate;

/**
 * A holder for response headers
 * @author ieb
 *
 */
public class ResponseHeaders {

	Map<String, List<String>> headers = new HashMap<String, List<String>>();

	/**
	 * Set a date header, removing all previous copies of the date header
	 * @param key the name of the date header
	 * @param timestamp GMT stimetamp as per System.currentTimeMillis()
	 */
	public void setDate(String key, long timestamp) {
		set(key, HttpDate.format(new Date(timestamp)));
	}

	/**
	 * Add another value to the date header.
	 * @param key the name of the date header
	 * @param timestamp the timestamp.
	 */
	public void addDate(String key, long timestamp) {
		getOrCreate(key).add(HttpDate.format(new Date(timestamp)));
	}

	/**
	 * Add a string header
	 * @param key the name of the string header
	 * @param value the value.
	 */
	public void add(String key, String value) {
		getOrCreate(key).add(value);
	}

	/**
	 * Replace the header
	 * @param key the name of the header.
	 * @param value the value.
	 */
	public void set(String key, long value) {
		set(key, String.valueOf(value));
	}

	/**
	 * Add a another copy of the date heaeder.
	 * @param key
	 * @param value
	 */
	public void add(String key, long value) {
		getOrCreate(key).add(String.valueOf(value));
	}

	/**
	 * Convert the request headers into a string suitable for sending to the client.
	 * @return
	 */
	public String toResponse() {
		StringBuilder sb = new StringBuilder();
		for (Entry<String, List<String>> e : headers.entrySet()) {
			for (String v : e.getValue()) {
				sb.append(e.getKey()).append(": ").append(v).append("\r\n");
			}
		}
		return sb.toString();
	}


	/**
	 * Set the header to a value replacing all other versions.
	 * @param key
	 * @param value
	 */
	public void set(String key, String value) {
		List<String> l = headers.get(key);
		if (l == null) {
			l = new ArrayList<String>();
			headers.put(key, l);
		} else {
			l.clear();
		}
		l.add(value);
	}

	private List<String> getOrCreate(String key) {
		List<String> l = headers.get(key);
		if (l == null) {
			l = new ArrayList<String>();
			headers.put(key, l);
		}
		return l;
	}

	public boolean contains(String key) {
		return headers.containsKey(key);
	}

}
