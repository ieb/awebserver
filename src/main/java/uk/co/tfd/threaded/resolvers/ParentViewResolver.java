package uk.co.tfd.threaded.resolvers;

import java.io.IOException;
import java.util.List;

import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Request;
import uk.co.tfd.threaded.View;
import uk.co.tfd.threaded.ViewResolver;

/**
 * Contains child resolvers processed in turn
 * @author ieb
 *
 */
public class ParentViewResolver implements ViewResolver {
	
	private List<ViewResolver> resolvers;
	private ViewResolver defaultViewResolver;

	public ParentViewResolver(List<ViewResolver> resolvers, ViewResolver defaultView) {
		this.resolvers = resolvers;
		this.defaultViewResolver = defaultView;
	}

	public View resolve(Request request) throws IOException, HttpRequestProtocolException {
		for ( ViewResolver v : resolvers ) {
			View view = v.resolve(request);
			if ( view != null ) {
				return view;
			}
		}
		return defaultViewResolver.resolve(request);
	}

}
