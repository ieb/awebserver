package uk.co.tfd.threaded.responses;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import org.apache.commons.lang.StringEscapeUtils;

import uk.co.tfd.Request;
import uk.co.tfd.ResponseHeaders;
import uk.co.tfd.threaded.RequestHandler;
import uk.co.tfd.util.CharSet;

public class HttpServerErrorResponse extends HttpResponse {

	private byte[] response;

	public HttpServerErrorResponse(Request request, Exception e ) {
		super(request, 500);
		ResponseHeaders h = new ResponseHeaders();		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintWriter pw = new PrintWriter(new OutputStreamWriter(baos, CharSet.UTF8));
		pw.print("<html><head></head><body><h1>Error:");
		pw.print(StringEscapeUtils.escapeHtml(e.getMessage()));
		pw.println("</h1><pre>");
		e.printStackTrace(pw);
		pw.println("</pre></body></html>");
		response = baos.toByteArray();
		h.set("Content-Type", "text/html; charset=utf8");
		h.set("Content-Length", response.length);
	}

	public void output(RequestHandler requestHandler) throws IOException {
		super.output(requestHandler);
		requestHandler.writeByte(response, 0, response.length);
	}

}
