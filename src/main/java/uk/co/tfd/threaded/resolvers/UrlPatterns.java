package uk.co.tfd.threaded.resolvers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.Request;
import uk.co.tfd.threaded.View;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

/**
 * Nested Url Resolver that produces views when presented with a request. It loads the url to view mappings
 * from a file. Each line in the file of of one of 2 forms.
 * 
 * ^/path/regex(?<withnamed>.*?)/(?<parameter>.*) : org.view.class.name
 * 
 * which will match 
 * 
 * /path/regex/2012/123kso13
 * 
 * invoke a new org.view.class(new Map<String,String>({ withnamed: 2012, parameter: 123kso13}));
 * 
 * or
 * 
 *  ^/path/prefix : include modulex/urlconfig.txt
 *  
 * Which will load modulex/urlconfig.txt using this class and prefix all the regexes in that file with ^/path/prefix
 * 
 * Once loaded the files are monitored and reloaded when they are changed.
 * 
 * 
 * @author ieb
 *
 */
public class UrlPatterns implements UrlPattern {

	protected static final Logger LOG = LoggerFactory.getLogger(UrlPatterns.class);
	private List<UrlPattern> patterns = ImmutableList.of();
	private long lastLoaded = 0;
	private String urlConfig;
	private TimerTask configMonitorTask;
	private String prefix;

	public UrlPatterns(String urlConfig, String prefix) throws IOException, SecurityException, ClassNotFoundException, NoSuchMethodException {
		this.urlConfig = urlConfig;
		this.prefix = prefix;
		reload();
		
		// monitor the file to reload it when it changes.
		configMonitorTask = new TimerTask() {
			
			@Override
			public void run() {
				try {
					reload();
				} catch (Exception e) {
					LOG.error(e.getMessage(),e);
				}
			}
		};
		SystemMonitor.schedule(configMonitorTask, 5000 );
	}


	public UrlPatterns(String urlConfig) throws SecurityException, IOException, ClassNotFoundException, NoSuchMethodException {
		this(urlConfig, null);
	}


	/**
	 * Reload the Url file.
	 * @throws IOException
	 * @throws NoSuchMethodException 
	 * @throws ClassNotFoundException 
	 * @throws SecurityException 
	 */
	private void reload() throws IOException, SecurityException, ClassNotFoundException, NoSuchMethodException {
		File f = new File(urlConfig);
		if (f.lastModified() > lastLoaded) {
			Builder<UrlPattern> b = ImmutableList.builder();
			BufferedReader br = new BufferedReader(new FileReader(f));
			try {
				while (true) {
					String l = br.readLine();
					if (l == null) {
						break;
					}
					String[] parts = l.split(":", 2);
					String pattern = parts[0].trim();
					if ( prefix != null ) {
						if ( pattern.startsWith("^")) {
							pattern = prefix + pattern.substring(1);
						} else {
							pattern = prefix + pattern;
						}
					}
					b.add(createUrlPattern(pattern, parts[1].trim()));
				}
			} finally {
				br.close();
			}
			lastLoaded = f.lastModified();
			List<UrlPattern> oldPatterns = patterns;
			patterns = b.build();
			for ( UrlPattern p : oldPatterns ) {
				p.release();
			}
		}
	}

	/**
	 * Create a url patter, which can either be a single url pattern or an included set of patterns.
	 * @param patternOrPrefix
	 * @param className
	 * @return
	 * @throws SecurityException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws NoSuchMethodException
	 */
	private UrlPattern createUrlPattern(String patternOrPrefix, String className) throws SecurityException, IOException, ClassNotFoundException, NoSuchMethodException {
		if ( className.startsWith("include ") ) {
			String includedFile = className.substring("include ".length());
			return new UrlPatterns(includedFile, patternOrPrefix);
		} else {
			return new SingleUrlPattern(patternOrPrefix, className);
		}
	}

	/* (non-Javadoc)
	 * @see uk.co.tfd.threaded.resolvers.UrlPattern#createView(uk.co.tfd.Request)
	 */
	public View createView(Request request) throws IllegalArgumentException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException {
		if ( prefix != null && !request.getPathInfo().startsWith(prefix) ) {
			return null; // doesn't start with prefix so can't be a match for this set.
		}
		for ( UrlPattern u : patterns ) {
			View v = u.createView(request);
			if ( v != null ) {
				return v;
			}
		}
		return null;
	}

	public void release() {
		configMonitorTask.cancel();
	}


}
