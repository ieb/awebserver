package uk.co.tfd.threaded.responses;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;

import junit.framework.Assert;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.Request;
import uk.co.tfd.event.responses.DummyRequest;
import uk.co.tfd.threaded.OutputHandler;
import uk.co.tfd.threaded.RequestHandler;
import uk.co.tfd.util.CharSet;
import uk.co.tfd.util.ParsedRequestLine;

public abstract class AbsrtractResponseTest {


	protected Request getRequest() {
		return new DummyRequest();
	}

	protected String responseTest(OutputHandler response) throws IOException {
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		response.output(new RequestHandler() {
			
			public void writeByte(byte[] buffer, int off, int len) throws IOException {
				baos.write(buffer, off, len);
			}
			
			public String readLine(int spaceBefore) throws IOException,
					HttpRequestException {
				fail("Wasnt expecting this to read");
				return null;
			}
			
			public int readByte(byte[] buffer, int off, int len) throws IOException {
				fail("Wasnt expecting this to read");
				return 0;
			}

			public void flush() throws IOException {				
			}
		});
		return new String(baos.toByteArray(),CharSet.UTF8);
	}
	

	protected String checkResponseString(String responseString, int code) throws IOException, HttpRequestException {
		for ( int i = 4; i < responseString.length()-1; i++) {
			if ( responseString.charAt(i) == '\r') {
				Assert.assertEquals('\n', responseString.charAt(i+1));
			}
			if ( responseString.charAt(i) == '\n') {
				Assert.assertEquals('\r', responseString.charAt(i-1));
			}
			if ("\r\n\r\n".equals(responseString.substring(i-4,i))) {
				break; // end of headers
			}
		}
		BufferedReader sr = new BufferedReader(new StringReader(responseString));
		String line = sr.readLine();
		ParsedRequestLine p = new ParsedRequestLine(line); // abusing this class
		String responseProtoVersion = p.getMethod();
		if ( !"HTTP/1.1".equals(responseProtoVersion) && !!"HTTP/1.0".equals(responseProtoVersion)  ) {
			Assert.fail("Response Status line invalid");
		}
		Assert.assertEquals(String.valueOf(code), p.getUri());
		Assert.assertFalse(line.endsWith("UNDEFINED"));
		Assert.assertFalse(line.endsWith("null"));
		String responseLine = line;
		while(true) {
			line = sr.readLine();
			if ( line == null ) break;
			if ( line.trim().length() == 0 ) {
				break; // end of headers
			}
			if (!line.startsWith(" ") ) {
				String[] parts = line.split(":",2);
				Assert.assertEquals(2, parts.length);
			}
		}
		return responseLine;
		
	}


}
