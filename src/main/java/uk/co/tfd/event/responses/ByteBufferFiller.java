package uk.co.tfd.event.responses;

import java.nio.ByteBuffer;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.event.InputHander;
import uk.co.tfd.event.RequestHandler;
import uk.co.tfd.event.Response;

/**
 * Fill a byte buffer from a channel as bytes become available. Doesnt handle
 * the case where the bytes become available faster thant they can be removed
 * from the transfer buffer. Since the source buffer is likely to be a file
 * thats going to be a must.
 * 
 * @author ieb
 * 
 */
public class ByteBufferFiller implements InputHander {

	private ByteBuffer transferBuffer;
	private long n;

	public ByteBufferFiller(ByteBuffer transferBuffer, long n) {
		this.transferBuffer = transferBuffer;
		this.n = n;
	}

	public Response update(ByteBuffer buffer) throws HttpRequestException {
		if (n <= 0) {
			return RequestHandler.TERMINATE;
		}
		if (buffer.limit() > n) {
			// handle input buffer overrun.
			byte[] b = new byte[(int) n];
			buffer.get(b);
			transferBuffer.put(b);
			n = 0;
			return RequestHandler.TERMINATE; // NB thats close the channel, not
												// the connection.
		}

		n = n - buffer.limit();
		transferBuffer.put(buffer);
		if (n == 0) {
			return RequestHandler.TERMINATE;
		}
		return RequestHandler.CONTINUE;
	}

}
