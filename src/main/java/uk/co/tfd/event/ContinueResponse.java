package uk.co.tfd.event;


/**
 * Continues the processing of bytes
 * @author ieb
 *
 */
public class ContinueResponse extends EmptyResponse {

	public boolean end() {
		return false;
	}

}
