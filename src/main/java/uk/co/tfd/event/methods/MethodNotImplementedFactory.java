package uk.co.tfd.event.methods;

import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Method;
import uk.co.tfd.MethodFactory;
import uk.co.tfd.Request;

public class MethodNotImplementedFactory implements MethodFactory {

	public Method create(Request request) throws HttpRequestProtocolException {
		return new MethodNotImplemented(request);
	}

}
