package uk.co.tfd;

import java.util.Iterator;

/**
 * Utility methods for iterables.
 * @author ieb
 *
 */
public class Iterables {

	/**
	 * @param k 
	 * @return an iterator encapsulating an iterable.
	 */
	public static <T> Iterable<T> iterable(final Iterator<T> k) {
		return new Iterable<T>(){
			public Iterator<T> iterator() {
				return k;
			}
			
		};
	}

	/**
	 * @return empty iterable
	 */
	public static <T> Iterable<T> empty() {
		return new Iterable<T>(){
			public Iterator<T> iterator() {
				return new Iterator<T>() {
					public boolean hasNext() {
						return false;
					}
					public T next() {
						return null;
					}
					public void remove() {
					}
				};
			}
			
		};
	}

}
