package uk.co.tfd.threaded.resolvers;

import java.lang.reflect.InvocationTargetException;

import uk.co.tfd.Request;
import uk.co.tfd.threaded.View;

/**
 * A UrlPattern is a pattern that will match a request and generate a view.
 * @author ieb
 *
 */
public interface UrlPattern {

	/**
	 * Generate a view from a request.
	 * @param request the request
	 * @return a view or null if the request didnt match.
	 * @throws IllegalArgumentException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	View createView(Request request) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException;

	/**
	 * Release all background resources including cancelling monitoring threads.
	 */
	void release();

}
