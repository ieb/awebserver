package uk.co.tfd;

/**
 * Indicates the client closed the stream.
 * @author ieb
 *
 */
public class HttpClientClosedStream extends HttpRequestException {

	public HttpClientClosedStream(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6756265375809882290L;

}
