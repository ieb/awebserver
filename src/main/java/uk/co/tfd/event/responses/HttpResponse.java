package uk.co.tfd.event.responses;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.Request;
import uk.co.tfd.ResponseHeaders;
import uk.co.tfd.Server;
import uk.co.tfd.event.OutputHandler;
import uk.co.tfd.event.Response;

public class HttpResponse implements OutputHandler {

	private static final Map<Integer, String> STATUS_CODES_10 = new HashMap<Integer, String>();
	private static final Map<Integer, String> STATUS_CODES_11 = new HashMap<Integer, String>();
	private static final Logger LOG = LoggerFactory
			.getLogger(ResponseHeaders.class);
	static {
		{
			loadCodes("http10codes.properties", STATUS_CODES_10);
			loadCodes("http11codes.properties", STATUS_CODES_11);
		}
	}

	public HttpResponse(Request request, int statusCode, ResponseHeaders h) {
		StringBuilder sb = new StringBuilder();
		String scm = STATUS_CODES_11.get(statusCode);
		if (request.isHttp10()) {
			scm = STATUS_CODES_10.get(statusCode);

		}
		if (scm == null) {
			scm = "HTTP/1.1 " + statusCode + " UNDEFINED";
		}
		sb.append(scm);
		sb.append(h.toResponse());
		sb.append("\r\n");
	}

	public HttpResponse(int statusCode) {
		StringBuilder sb = new StringBuilder();
		String scm = STATUS_CODES_11.get(statusCode);
		ResponseHeaders h = new ResponseHeaders();
		h.setDate("Date", System.currentTimeMillis());
		h.set("Server", Server.VERSION);
		h.set("Content-Length", 0); // not sure if this is required, but might need to be to allow keep alives.
		sb.append(scm);
		sb.append(h.toResponse());
		sb.append("\r\n");
	}

	private static void loadCodes(String codesFile,
			Map<Integer, String> statusCodes) {

		try {
			Properties p = new Properties();
			p.load(HttpResponse.class
					.getResourceAsStream(codesFile));
			for (Object k : p.keySet()) {
				statusCodes.put(Integer.parseInt((String) k),
						p.getProperty((String) k));
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	public Response fillOutputBuffer(ByteBuffer buffer) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
