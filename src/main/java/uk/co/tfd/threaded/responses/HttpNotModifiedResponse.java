package uk.co.tfd.threaded.responses;

import uk.co.tfd.Request;


/**
 * A 304 response, not modified.
 * @author ieb
 *
 */
public class HttpNotModifiedResponse extends HttpResponse {

	public HttpNotModifiedResponse(Request request) {
		super(request, 304);
	}

}
