package uk.co.tfd.threaded;

import java.io.IOException;

import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Request;

/**
 * Resolves requests into views.
 * @author ieb
 *
 */
public interface ViewResolver {

	/**
	 * @param request the request
	 * @return a view 
	 * @throws IOException 
	 * @throws HttpRequestProtocolException 
	 */
	View resolve(Request request) throws IOException, HttpRequestProtocolException;

}
