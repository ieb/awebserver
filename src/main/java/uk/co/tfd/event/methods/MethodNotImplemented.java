package uk.co.tfd.event.methods;

import java.nio.ByteBuffer;

import uk.co.tfd.Request;
import uk.co.tfd.event.EventMethod;
import uk.co.tfd.event.RequestHandler;
import uk.co.tfd.event.Response;

public class MethodNotImplemented implements EventMethod {

	public MethodNotImplemented(Request request) {
		// TODO Auto-generated constructor stub
	}

	public Response processBody(ByteBuffer buffer) {
		return RequestHandler.CONTINUE;
	}

	public Response endOfHeaders() {
		return RequestHandler.CONTINUE;
	}

	public Response fillOutputBuffer(ByteBuffer buffer) {
		// fill the buffer with a Method not implemented response.
		return RequestHandler.TERMINATE;
	}

}
