package uk.co.tfd.util;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Test;

public class HttpDateTest {

	@Test
	public void testReoundtrip() {
		Date d1 = HttpDate.parse("Sun, 06 Nov 1994 08:49:37 GMT");
		Date d2 = HttpDate.parse("Sunday, 06-Nov-94 08:49:37 GMT");
		Date d3 = HttpDate.parse("Sun Nov  6 08:49:37 1994");
		assertEquals(d1, d2);
		assertEquals(d1, d3);
		String dateFormatted = HttpDate.format(d1);
		assertEquals("Sun, 06 Nov 1994 08:49:37 +0000", dateFormatted);
	}

	@Test
	public void testOdDates() {
		HttpDate.parse("Sun, 6 Nov 1994 08:49:37 GMT");
		HttpDate.parse("Sun, 6 Nov 1994 08:49:37 +1012");
		HttpDate.parse("Sunday, 06-Nov-94 08:49:37 EST");
		HttpDate.parse("Sunday, 06-Nov-94 08:49:37 BST");
	}

}
