package uk.co.tfd.threaded.methods;

import uk.co.tfd.threaded.ViewResolver;


public class HeadMethodFactory extends GetMethodFactory {

	public HeadMethodFactory(ViewResolver viewResolver) {
		super(viewResolver);
	}
}