package uk.co.tfd.event.responses;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import uk.co.tfd.Request;
import uk.co.tfd.ResponseHeaders;
import uk.co.tfd.event.OutputHandler;
import uk.co.tfd.event.RequestHandler;
import uk.co.tfd.event.Response;
import uk.co.tfd.util.ContentType;

public class FileBodyResponse implements OutputHandler {

	private FileChannel channel;
	private FileInputStream fileIn;
	private long toread;
	private HttpResponse responseHeaders;

	public FileBodyResponse(Request request, final File requestedFile) throws IOException {
		fileIn = new FileInputStream(requestedFile);
		toread = requestedFile.length();
		channel = fileIn.getChannel();
		ResponseHeaders h = new ResponseHeaders();
		h.setDate("Last-Modified",requestedFile.lastModified());
		h.set("Content-Length",toread);
		h.set("Content-Type",ContentType.toString(requestedFile));
		responseHeaders = new HttpResponse(request, 200,h);
	}


	public Response fillOutputBuffer(ByteBuffer buffer) throws IOException {
		responseHeaders.fillOutputBuffer(buffer);
		int n = channel.read(buffer);
		if ( n == -1 ) {
			channel.close();
			fileIn.close();
			return RequestHandler.TERMINATE;
		}
		toread = toread - n;
		if ( n < 0 ) {
			return RequestHandler.TERMINATE;			
		}
		if ( n == 0 ) {
			channel.close();
			fileIn.close();			
			return RequestHandler.TERMINATE;
		}
		return RequestHandler.CONTINUE;
	}

}
