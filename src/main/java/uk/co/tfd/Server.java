package uk.co.tfd;

/**
 * Constants related to the server.
 * @author ieb 
 * 
 *
 */
public class Server {

	/**
	 * The version.
	 */
	public static final String VERSION = "AWebServer/0.1"; // spec 3.8

}
