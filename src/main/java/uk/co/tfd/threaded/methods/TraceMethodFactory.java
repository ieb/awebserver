package uk.co.tfd.threaded.methods;


/**
 * Trace method, which we will leave disabled.
 * @author ieb
 *
 */
public class TraceMethodFactory extends MethodNotImplementedFactory {
}
