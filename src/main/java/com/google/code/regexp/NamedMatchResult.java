package com.google.code.regexp;
/**
 * Taken from http://code.google.com/p/named-regexp/
 * Licensed under ASL2
 * @author clement.denis
 * No maven repo release available.
 * Used while not on Java 7 which has named regex support.
 */
import java.util.List;
import java.util.Map;
import java.util.regex.MatchResult;

public interface NamedMatchResult extends MatchResult {

        public List<String> orderedGroups();

        public Map<String, String> namedGroups();

        public String group(String groupName);

        public int start(String groupName);

        public int end(String groupName);

}