package uk.co.tfd;

import java.util.HashMap;

/**
 * Holds method factories keyed by the the method name.
 * @author ieb
 *
 */
public class MethodRegistry {
	


	private HashMap<String, MethodFactory> registry;
	private int maxKeyLength;
	

	public MethodRegistry() {
		registry = new HashMap<String, MethodFactory>();
	}
	
	public void put(String key, MethodFactory methodFactory) {
		maxKeyLength = Math.max(key.length(), maxKeyLength);
		registry.put(key, methodFactory);
		
	}

	public int getMaxMethodLength() {
		return maxKeyLength;
	}

	public MethodFactory get(String key) throws HttpRequestProtocolException {
		MethodFactory mf = registry.get(key);
		if ( mf == null ) {
			throw new HttpRequestProtocolException(501);
		}
		return mf;
	}
	

}
