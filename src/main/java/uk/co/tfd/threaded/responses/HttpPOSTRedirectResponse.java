package uk.co.tfd.threaded.responses;

import uk.co.tfd.ResponseHeaders;
import uk.co.tfd.Request;

/**
 * A post redirect, see the spec for more details and rational on why.
 * 
 * @author ieb
 * 
 */
public class HttpPOSTRedirectResponse extends HttpResponse {
	public HttpPOSTRedirectResponse(Request request, String location) {
		super(request, 303, setLocation(request, location));
	}

	private static ResponseHeaders setLocation(Request request, String location) {
		ResponseHeaders h = new ResponseHeaders();
		h.set("Location", location);
		return h;
	}

}
