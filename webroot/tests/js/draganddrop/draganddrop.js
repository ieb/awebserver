var console;
define(["jquery",
        "jquery-ui"
    ], 
function($){
	"use strict";
	
	console.log("Making item dragable");
	$("#tobedragged").draggable();
	console.log("Done Making item dragable");
	$("#drophere").droppable({
		drop: function(event, ui) {
			console.log("Item was dropped ");
			$.post('#',{
					  'element' : $("#tobedragged").html()
				  },
				  function() {
						$("#tobedragged").fadeOut();
						$("#drophere").text("Gone!, posted Ok");					  
				  }
				).error(
				  function() {
						$("#drophere").text("Ooops something went wrong, try again");
				  });
		}
	});
});
