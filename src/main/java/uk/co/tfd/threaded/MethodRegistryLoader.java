package uk.co.tfd.threaded;

import java.util.ArrayList;
import java.util.List;

import uk.co.tfd.MethodRegistry;
import uk.co.tfd.threaded.methods.ConnectMethodFactory;
import uk.co.tfd.threaded.methods.DeleteMethodFactory;
import uk.co.tfd.threaded.methods.GetMethodFactory;
import uk.co.tfd.threaded.methods.HeadMethodFactory;
import uk.co.tfd.threaded.methods.OptionsMethodFactory;
import uk.co.tfd.threaded.methods.PostMethodFactory;
import uk.co.tfd.threaded.methods.PutMethodFactory;
import uk.co.tfd.threaded.methods.TraceMethodFactory;
import uk.co.tfd.threaded.resolvers.DefaultViewResolver;
import uk.co.tfd.threaded.resolvers.FileSystemViewResolver;
import uk.co.tfd.threaded.resolvers.ParentViewResolver;

/**
 * Loads standard MethodFactories into the MethodRegistry.
 * @author ieb
 *
 */
public class MethodRegistryLoader {
	

	public static MethodRegistry getFactories() {
		MethodRegistry mf = new MethodRegistry();
		/* 5.1.1 Method
		 * Method         = "OPTIONS"                ; Section 9.2
         *             | "GET"                    ; Section 9.3
         *             | "HEAD"                   ; Section 9.4
         *             | "POST"                   ; Section 9.5
         *             | "PUT"                    ; Section 9.6
         *             | "DELETE"                 ; Section 9.7
         *             | "TRACE"                  ; Section 9.8
         *             | "CONNECT"                ; Section 9.9
         *             | extension-method
		 */
		List<ViewResolver> resolvers = new ArrayList<ViewResolver>();
		// resolvers.add(new RegexViewResolver("urls.config", null));
		resolvers.add(new FileSystemViewResolver("webroot"));
		ViewResolver viewResolver = new ParentViewResolver(resolvers, new DefaultViewResolver());
		mf.put("OPTIONS", new OptionsMethodFactory());
		mf.put("GET", new GetMethodFactory(viewResolver));
		mf.put("HEAD", new HeadMethodFactory(viewResolver));
		mf.put("POST", new PostMethodFactory());
		mf.put("PUT", new PutMethodFactory());
		mf.put("DELETE", new DeleteMethodFactory());
		mf.put("TRACE", new TraceMethodFactory());
		mf.put("CONNECT", new ConnectMethodFactory());
		return mf;
	}

}
