package uk.co.tfd.threaded;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Request;
import uk.co.tfd.threaded.methods.Form;
import uk.co.tfd.util.ParsedRequestLine;

/**
 * An implementation of Request for a threaded environment.
 * @author ieb
 *
 */
public class ThreadedRequest implements Request {

	private static final Logger LOG = LoggerFactory
			.getLogger(ThreadedRequest.class);
	/**
	 * The request line, parsed.
	 */
	private ParsedRequestLine parsedRequestLine;
	/**
	 * A storage property for the current header being parsed.
	 */
	private StringBuilder currentHeader = new StringBuilder();
	/**
	 * All the headers.
	 */
	private Map<String, List<String>> headers = new HashMap<String, List<String>>();
	/**
	 * True if the connection should be kept open.
	 */
	private boolean shouldKeepAlive = true;
	/**
	 * The length of the body of this request remaining, or -1 if read to end of
	 * stream
	 */
	private int contentLength = -1;
	/**
	 * Force the connection to close after this request.
	 */
	private boolean forceClose;
	/**
	 * When the request was started.
	 */
	private long startTime;
	/**
	 * The request handler associated with this request.
	 */
	private RequestHandler requestHandler;
	private Form form;

	public ThreadedRequest(RequestHandler requestHandler,
			ParsedRequestLine parsedRequestLine, long endKeepAlive,
			int nrequests) throws IOException, HttpRequestException {
		this.requestHandler = requestHandler;
		startTime = System.currentTimeMillis();
		this.parsedRequestLine = parsedRequestLine;
		String line = null;
		// parse the request headers, until we get a blank line, including
		// dealing with continuation lines.
		while (true) {
			try {
				line = requestHandler.readLine(0);
			} catch (IOException ex) {
				throw new HttpRequestProtocolException(400);
			}
			if (line == null || line.length() == 0) {
				saveHeader("");
				break;
			}
			// HTTP/1.1 4.2 says at least one SP or HT, so how do we know if there is a space required. Assuming
			// we only remove the first SP or HT.
			if (line.startsWith(" ") || line.startsWith("\t")) {
				addToHeader(line.substring(1));
			} else {
				saveHeader(line);
			}
		}
		setupBodyLength();
		if (System.currentTimeMillis() + 10 > endKeepAlive || nrequests == 0) {
			forceClose = true;
		}
		checkKeepAlive(false); // assume we wont be reading the body. The
								// ThreadedMethod impl should call this with
								// true if that is not the case.
		form = new Form(this, requestHandler);
	}

	public void checkKeepAlive(boolean expectsBody) {
		if (isHttp10()) {
			shouldKeepAlive = false; // HTTP1.0 didnt have persistent
										// connections, so close.
			return;
		}
		if (forceClose) {
			shouldKeepAlive = false; // this connections time has ended.
			return;
		}
		if (headers.containsKey("Connection")) {
			for (String v : headers.get("Connection")) {
				if ("close".equalsIgnoreCase(v)) {
					shouldKeepAlive = false; // client said close.
					return;
				}
			}
		}
		if (expectsBody && contentLength != -1) {
			shouldKeepAlive = false; // have to read to the end of the stream,
										// so cant keep it open.
			return;
		}
		shouldKeepAlive = true;
	}

	public String getRequestLine() {
		return parsedRequestLine.getRequestLine();
	}

	public Map<String, List<String>> getHeaders() {
		return headers;
	}

	public String getMethodName() {
		return parsedRequestLine.getMethod();
	}

	public String getPathInfo() {
		return parsedRequestLine.getPathInfo();
	}

	public Map<String, List<String>> getRequestParameters() throws HttpRequestException, IOException {
		if ( "POST".equals(getMethodName())) {
			return form.getParams();
		}
		return parsedRequestLine.getParams();
	}
	

	public String getQueryString() {
		return parsedRequestLine.getQueryString();
	}

	public String getProtocolVersion() {
		// Make compliant with 3.1 of the spec
		return parsedRequestLine.getProtocolVersion();
	}

	public boolean isHttp10() {
		return "1.0".equals(parsedRequestLine.getProtocolVersion());
	}

	public boolean shouldKeepAlive() {
		return shouldKeepAlive;
	}

	/**
	 * How many bytes should be read, based on number available in the body and
	 * the buffer lenght.
	 * 
	 * @param len
	 * @return
	 */
	public int getBytesToRead(int len) {
		if (contentLength == -1) {
			return len;
		}
		LOG.error("Body has {} bytes left ", contentLength);
		return Math.min(len, contentLength);
	}

	/**
	 * Records how many were read.
	 * 
	 * @param read
	 * @return
	 */
	public int adjustBodyRead(int read) {
		if (contentLength == -1) {
			return read;
		}
		if ( contentLength == 0 ) {
			return -1;
		}
		if (read == -1) {
			contentLength = -1;
		} else {
			contentLength = contentLength - read;
		}
		return read;
	}

	public boolean done(int statusCode) {
		LOG.info("{} {} {} {} ms", new Object[] { getMethodName(), statusCode,
				getPathInfo(), getTime() });
		return shouldKeepAlive;
	}

	/**
	 * Add to the header
	 * 
	 * @param line
	 */
	private void addToHeader(String line) {
		currentHeader.append(line.trim());
	}

	/**
	 * Save and parse the header.
	 * 
	 * @param line
	 */
	private void saveHeader(String line) {
		addToHeader(line);
		if (currentHeader.length() == 0) {
			return;
		}
		String[] header = currentHeader.toString().split(":", 2);
		String value = "";
		if (header.length == 2) {
			value = header[1].trim();
		}
		String k = header[0].trim();
		if (headers.containsKey(k)) {
			headers.get(k).add(value);
		} else {
			ArrayList<String> l = new ArrayList<String>();
			l.add(value);
			headers.put(k, l);
		}
		currentHeader.setLength(0);
	}

	/**
	 * Work out what the body length will be from request headers. This is used
	 * to control reading of the body.
	 */
	private void setupBodyLength() {
		contentLength = -1;
		if (headers.containsKey("Content-Length")) {
			for (String v : headers.get("Content-Length")) {
				contentLength = Integer.parseInt(v);
			}
		}
	}

	/**
	 * @return how long in ms has the request been running.
	 */
	private long getTime() {
		return System.currentTimeMillis() - startTime;
	}

	public Form getForm() {
		return form;
	}

}
