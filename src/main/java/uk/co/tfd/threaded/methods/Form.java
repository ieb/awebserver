package uk.co.tfd.threaded.methods;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Request;
import uk.co.tfd.threaded.RequestHandler;

/**
 * Parses a POST request body.
 * @author ieb
 *
 */
public class Form {

	private static final Logger LOG = LoggerFactory.getLogger(Form.class);
	private boolean standardForm;
	private String charset;
	private Map<String, List<String>> formParams = new HashMap<String, List<String>>();
	private boolean loaded;
	private RequestHandler requestHandler;

	public Form(Request request, RequestHandler requestHandler)
			throws IOException, HttpRequestException {
		this.requestHandler = requestHandler;
		standardForm = false;
		loaded = false;
		// Work out what type of form we are dealing with,
		// if its application/x-www-form-urlencoded; charset=UTF-8
		Map<String, List<String>> headers = request.getHeaders();

		if (headers.containsKey("Content-Type")) {
			String contentType = headers.get("Content-Type").get(0);
			String[] parts = contentType.split(";");
			contentType = parts[0];
			Map<String, String> params = new HashMap<String, String>();
			for (int i = 1; i < parts.length; i++) {
				String[] paramparts = parts[i].split("=");
				params.put(paramparts[0].trim(), paramparts[1].trim());
			}
			charset = "UTF-8"; // maybe this should is ISO-8859-1,
								// but hardly anyone posts in that
								// now.
			if (params.containsKey("charset")) {
				charset = params.get("charset");
			}
			standardForm = true;
		}
	}

	private void readPost() throws HttpRequestException, IOException {
		byte[] b = new byte[2048];
		try {

			StringBuffer sb = new StringBuffer();
			while (true) {
				int n = requestHandler.readByte(b, 0, b.length);
				if (n < 0)
					break;
				sb.append(new String(b, 0, n, "UTF-8")); // it wont be more than
															// ascii (%
															// encoding)
			}

			decodePost(sb.toString());

		} catch (UnsupportedEncodingException e) {
			throw new HttpRequestProtocolException(400);
		} finally {
			try {
				while (true) {
					int n = requestHandler.readByte(b, 0, b.length);
					if (n < 0)
						break;
				}
			} catch (Exception e) {
				LOG.error("Failed to read to end of buffer ");
				throw new HttpRequestException(
						"Bad Unrecoverable Post, terminating connection");

			}

		}
	}

	private void decodePost(String postBody)
			throws UnsupportedEncodingException {
		StringTokenizer st = new StringTokenizer(postBody, "&");
		while (st.hasMoreTokens()) {
			String[] parts = st.nextToken().split("=", 2);

			parts[0] = URLDecoder.decode(parts[0], charset);
			parts[1] = URLDecoder.decode(parts[1], charset);
			addFormParam(parts[0], parts[1]);
		}
	}

	private void addFormParam(String key, String value) {
		List<String> v = formParams.get(key);
		if (v == null) {
			v = new ArrayList<String>();
			formParams.put(key, v);
		}
		v.add(value);
	}

	public boolean isStandardForm() {
		return standardForm;
	}

	public Map<String, List<String>> getParams() throws HttpRequestException, IOException {
		if (!loaded) {
			readPost();
			loaded = true;
		}
		return formParams;
	}

}
