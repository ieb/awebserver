package uk.co.tfd.event.methods;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import uk.co.tfd.Request;
import uk.co.tfd.event.EventMethod;
import uk.co.tfd.event.OutputHandler;
import uk.co.tfd.event.RequestHandler;
import uk.co.tfd.event.Response;
import uk.co.tfd.event.responses.DirectoryResponse;
import uk.co.tfd.event.responses.FileBodyResponse;
import uk.co.tfd.event.responses.HttpForbiddenResponse;
import uk.co.tfd.event.responses.HttpNotFoundResponse;

public class GetMethod implements EventMethod {


	private Request request;
	private OutputHandler outputHandler;

	public GetMethod(Request request) {
		this.request = request;
	}

	public Response processBody(ByteBuffer buffer) {
		return RequestHandler.CONTINUE;
	}

	public Response endOfHeaders() throws IOException {
		File webroot = new File("webroot");
		File requestedFile = new File(webroot,request.getPathInfo());
		if ( requestedFile.getAbsolutePath().startsWith(webroot.getAbsolutePath())) {
			if ( requestedFile.isFile() ) {
				outputHandler = new FileBodyResponse(request, requestedFile);
			} else if ( requestedFile.isDirectory() ) {
				outputHandler = new DirectoryResponse(request, requestedFile);
			} else {
				outputHandler = new HttpNotFoundResponse(request);
			}
		}
		outputHandler = new HttpForbiddenResponse();
		return RequestHandler.CONTINUE;
	}

	public Response fillOutputBuffer(ByteBuffer buffer) throws IOException {
		return outputHandler.fillOutputBuffer(buffer);
	}

}
