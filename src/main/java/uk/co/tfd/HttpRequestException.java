package uk.co.tfd;

/**
 * A general response 
 * @author ieb
 *
 */
public class HttpRequestException extends Exception {

	public HttpRequestException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -5640197573262264284L;

}
