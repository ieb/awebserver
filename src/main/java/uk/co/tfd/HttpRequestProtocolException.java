package uk.co.tfd;

/**
 * Indicates that there was a protocol error with the request. The staus code
 * indicates the nature of the error.
 * 
 * @author ieb
 * 
 */
public class HttpRequestProtocolException extends HttpRequestException {

	private int statusCode;

	public HttpRequestProtocolException(int statusCode) {
		super("Error Status Code:" + statusCode);
		this.statusCode = statusCode;

	}

	public HttpRequestProtocolException(int statusCode, String message) {
		super(message);
		this.statusCode = statusCode;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5033668215345164222L;

	public int getCode() {
		return statusCode;
	}

}
