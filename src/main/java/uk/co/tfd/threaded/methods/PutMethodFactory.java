package uk.co.tfd.threaded.methods;


/**
 * Not supporting puts from clients at the moment.
 * @author ieb
 *
 */
public class PutMethodFactory extends MethodNotImplementedFactory {
}
