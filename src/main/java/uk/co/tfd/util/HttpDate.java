package uk.co.tfd.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Writes RFC 1123 formatted dates
 * Reads RFC 1123, 822, 850 1036 and Ansi C dates as per HTTP 1.1 Spec para 3.3
 *	  Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
 *    Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
 *    Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format
 * @author ieb
 *
 */
public class HttpDate {

	private static final String RFC1123_PATTERN = "EEE, dd MMM yyyy HH:mm:ss Z";	 // Wed, 04 Jul 2001 12:08:56 -0700;
	private static final String[] PARSING_PATTERNS = new String[]{
		"EEE, dd MMM yyyy HH:mm:ss Z", //  RFC 822, updated by RFC 1123
		"EEE, d MMM yyyy HH:mm:ss Z", //  RFC 822, updated by RFC 1123 with non 0 delimited date
		"EEE, dd MMM yyyy HH:mm:ss z", //  RFC 822, updated by RFC 1123 with non RFC822 timezone 
		"EEE, d MMM yyyy HH:mm:ss z", //  RFC 822, updated by RFC 1123 with non RFC822 timezone delimited date
		"EEEE, dd-MMM-yy HH:mm:ss Z", //  RFC 850, obsoleted by RFC 1036
		"EEEE, dd-MMM-yy HH:mm:ss z", //  RFC 850, obsoleted by RFC 1036
		"EEE MMM d HH:mm:ss yyyy" // ANSI C's asctime() format (with a TZ added which will be GMT)
		
	};
	
	private static final Logger LOG = LoggerFactory.getLogger(HttpDate.class);

	public static String format(Date date) {
		// SimpleDateFormatter is not thread safe :(
		SimpleDateFormat formatter = new SimpleDateFormat(RFC1123_PATTERN, Locale.US);
		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		return formatter.format(date);
	}

	public static Date parse(String dateString) {
		for (String p : PARSING_PATTERNS) {
			try {
				SimpleDateFormat parser = new SimpleDateFormat(p, Locale.US);
				parser.setTimeZone(TimeZone.getTimeZone("GMT"));
				return parser.parse(dateString);
			} catch (ParseException e) {
				LOG.debug("Failed to parse {} with {} ", dateString, p);
			}
		}
		throw new IllegalArgumentException(
				"Invalid date format for HTTP spec, please see RFC http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.3 ");
	}	
	
	
	
     
     
}
