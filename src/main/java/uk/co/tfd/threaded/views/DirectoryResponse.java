package uk.co.tfd.threaded.views;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.Request;
import uk.co.tfd.ResponseHeaders;
import uk.co.tfd.threaded.OutputHandler;
import uk.co.tfd.threaded.RequestHandler;
import uk.co.tfd.threaded.View;
import uk.co.tfd.threaded.responses.HttpResponse;
import uk.co.tfd.util.CharSet;
import uk.co.tfd.util.HttpDate;

/**
 * Creates a directory response, redirecting to an index file if one is present.
 * @author ieb
 *
 */
public class DirectoryResponse implements OutputHandler, View {

	private static final String[] INDEX_FILES = new String[] { "index.html",
			"index.htm" };
	private static final Logger LOG = LoggerFactory
			.getLogger(DirectoryResponse.class);
	private static final String HTML_HEADER = loadFragment("dirheader.html");
	private static final String MESSAGE_LINE = loadFragment("dirline.html");
	private static final String HTML_FOOTER = loadFragment("dirfooter.html");
	private OutputHandler delegate;
	private HttpResponse responseHeaders;
	private byte[] buffer;
	private File requestedFile;
	private boolean no_body;

	/**
	 * Generate a directory response
	 * @param request the request
	 * @param requestedFile the location
	 * @param head true if a HEAD response is required.
	 * @throws IOException
	 */
	public DirectoryResponse(Request request, final File requestedFile)
			throws IOException {
		this.requestedFile = requestedFile;
		for (String f : INDEX_FILES) {
			File inf = new File(requestedFile, f);
			if (inf.isFile()) {
				delegate = new FileBodyResponse(request, inf);
				return;
			}
		}

		ResponseHeaders h = new ResponseHeaders();
		h.setDate("Last-Modified", requestedFile.lastModified());
		h.set("Content-Type", "text/html; charset=utf-8");
		buffer = getDirectoryListing();
		// the spec 9.4 says "The metainformation contained in the HTTP headers in
		// response to a HEAD request SHOULD be identical to the information sent in
		// response to a GET request. "
		// So we must set the Content-Length.
		// Clients reading having sent the HEAD request should not attempt to read the body.
		// libcurl is broken when invoked with curl -X HEAD ...
		h.set("Content-Length", buffer.length);
		responseHeaders = new HttpResponse(request, 200, h);
		no_body = "HEAD".equals(request.getMethodName());
	}

	/**
	 * Loads a fragment HTML file as a string.
	 * @param name the name of the fragment in the classpath in the same package.
	 * @return the fragment.
	 */
	private static String loadFragment(String name) {
		BufferedReader in = new BufferedReader(new InputStreamReader(
				DirectoryResponse.class.getResourceAsStream(name)));
		try {
			StringBuilder sb = new StringBuilder();
			while (true) {
				String line = in.readLine();
				if (line == null) {
					break;
				}
				sb.append(line);
			}
			return sb.toString();
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			return "";
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * @return a byte[] of the directory listing.
	 */
	private byte[] getDirectoryListing() {
		StringBuilder sb = new StringBuilder();
		sb.append(HTML_HEADER);
		for ( File f : requestedFile.listFiles()) {
			sb.append(MessageFormat.format(MESSAGE_LINE, f.getName(), f.length(), HttpDate.format(new Date(f.lastModified()))));
		}
		sb.append(HTML_FOOTER);
		return sb.toString().getBytes(CharSet.UTF8);
	}

	/**
	 * Output the directory listing or delegate to an index file.
	 * 
	 * @see uk.co.tfd.threaded.OutputHandler#output(uk.co.tfd.threaded.RequestHandler)
	 */
	public void output(RequestHandler requestHandler) throws IOException {
		if (delegate != null) {
			delegate.output(requestHandler);
		} else {
			responseHeaders.output(requestHandler);
			if ( no_body ) {
				return;
			}
			requestHandler.writeByte(buffer, 0, buffer.length);
		}
	}

	/**
	 * Get the status code from the response or delegate.
	 * @see uk.co.tfd.threaded.OutputHandler#getStatusCode()
	 */
	public int getStatusCode() {
		if ( delegate != null ) {
			return delegate.getStatusCode();
		} 
		return responseHeaders.getStatusCode();
	}

	public OutputHandler getOutputHander() {
		return this;
	}

}
