package uk.co.tfd.event;

/**
 * Represents a response object, that may tell the event driver abandon processing.
 * @author ieb
 *
 */
public interface Response {

	/**
	 * @return true if the response processing should be stopped
	 */
	boolean end();

	/**
	 * @return An iterable of event sources that need to be registered with the event loop.
	 */
	Iterable<EventSource> getEventSources();

}
