package uk.co.tfd.event;

import uk.co.tfd.MethodRegistry;
import uk.co.tfd.event.methods.ConnectMethodFactory;
import uk.co.tfd.event.methods.DeleteMethodFactory;
import uk.co.tfd.event.methods.GetMethodFactory;
import uk.co.tfd.event.methods.HeadMethodFactory;
import uk.co.tfd.event.methods.OptionsMethodFactory;
import uk.co.tfd.event.methods.PostMethodFactory;
import uk.co.tfd.event.methods.PutMethodFactory;
import uk.co.tfd.event.methods.TraceMethodFactory;

public class MethodRegistryLoader {
	public static MethodRegistry getRegistry() {
		MethodRegistry registry = new MethodRegistry();
		/* 5.1.1 Method
		 * Method         = "OPTIONS"                ; Section 9.2
         *             | "GET"                    ; Section 9.3
         *             | "HEAD"                   ; Section 9.4
         *             | "POST"                   ; Section 9.5
         *             | "PUT"                    ; Section 9.6
         *             | "DELETE"                 ; Section 9.7
         *             | "TRACE"                  ; Section 9.8
         *             | "CONNECT"                ; Section 9.9
         *             | extension-method
		 */
		registry.put("OPTIONS", new OptionsMethodFactory());
		registry.put("GET", new GetMethodFactory());
		registry.put("HEAD", new HeadMethodFactory());
		registry.put("POST", new PostMethodFactory());
		registry.put("PUT", new PutMethodFactory());
		registry.put("DELETE", new DeleteMethodFactory());
		registry.put("TRACE", new TraceMethodFactory());
		registry.put("CONNECT", new ConnectMethodFactory());
		return registry;
	}


}
