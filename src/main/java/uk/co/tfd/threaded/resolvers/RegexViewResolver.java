package uk.co.tfd.threaded.resolvers;

import java.io.IOException;

import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Request;
import uk.co.tfd.threaded.View;
import uk.co.tfd.threaded.ViewResolver;

/**
 * Loads regexes from a file and resolves views.
 * @author ieb
 *
 */
public class RegexViewResolver implements ViewResolver {

	private ViewResolver defaultViewResolver;
	private UrlPatterns urlPatterns;
	public RegexViewResolver(String urlConfig, ViewResolver defaultView) throws SecurityException, IOException, ClassNotFoundException, NoSuchMethodException {
		urlPatterns = new UrlPatterns(urlConfig);
		
	}
	public View resolve(Request request) throws IOException, HttpRequestProtocolException {
		
		try {
			View v =  urlPatterns.createView(request);
			if ( v != null ) {
				return v;
			}
			return defaultViewResolver.resolve(request);
		} catch (Exception e) {
			throw new HttpRequestProtocolException(500,"Unable to process request "+e.getMessage());
		}
	}

}
