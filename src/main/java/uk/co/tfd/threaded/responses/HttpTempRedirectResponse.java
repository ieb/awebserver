package uk.co.tfd.threaded.responses;

import uk.co.tfd.ResponseHeaders;
import uk.co.tfd.Request;


/**
 * A temp redirect.
 * @author ieb
 *
 */
public class HttpTempRedirectResponse extends  HttpResponse {
	public HttpTempRedirectResponse(Request request, String location) {
		super(request, 302, setLocation(request, location));
	}

	private static ResponseHeaders setLocation(Request request, String location) {
		ResponseHeaders h = new ResponseHeaders();
		h.set("Location", location);
		return h;
	}

}
