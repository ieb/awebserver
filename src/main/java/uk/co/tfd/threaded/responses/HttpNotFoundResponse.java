package uk.co.tfd.threaded.responses;

import uk.co.tfd.Request;


/**
 * A 404 response, resource not found.
 * @author ieb
 *
 */
public class HttpNotFoundResponse extends HttpResponse {
	
	public HttpNotFoundResponse(Request request) {
		super(request, 404);
	}

}
