package uk.co.tfd.threaded.responses;

import uk.co.tfd.Request;


/**
 * A 100 continue response
 * @author ieb
 *
 */
public class HttpContinueResponse extends HttpResponse {
	
	public HttpContinueResponse(Request request) {
		super(request, 100);
	}

}
