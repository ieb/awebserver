package uk.co.tfd.threaded.methods;

import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Method;
import uk.co.tfd.MethodFactory;
import uk.co.tfd.Request;
import uk.co.tfd.threaded.ViewResolver;


public class GetMethodFactory  implements MethodFactory {

	private ViewResolver viewResolver;
	
	public GetMethodFactory(ViewResolver viewResolver) {
		this.viewResolver = viewResolver;
	}

	public Method create(Request request) throws HttpRequestProtocolException {
		return new GetMethod(request, viewResolver);
	}
}
