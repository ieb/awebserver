package uk.co.tfd.event;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.ResponseHeaders;
import uk.co.tfd.HttpRequestException;
import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.MethodFactory;
import uk.co.tfd.MethodRegistry;
import uk.co.tfd.Request;
import uk.co.tfd.util.ParsedRequestLine;

/**
 * This request hander is an byte[] sync. The update method is called with bytes
 * as they arrive and this changes the state of the request hander, which
 * processes the stream of bytes. Initially by processing the request line, then
 * by processing the headers and after getting a blank CRNL, assuming the
 * request was recognized, the bytes are routed to a Method. At the end of each
 * byte[] a Response is returned which tells the driving event loop what to do
 * next.
 * 
 * @author ieb
 * 
 */
public class RequestHandler implements InputHander, OutputHandler, Request {

	private static final Logger LOG = LoggerFactory
			.getLogger(RequestHandler.class);
	/**
	 * A standard continue response, that will cause the event loop to continue.
	 */
	public static final Response CONTINUE = new ContinueResponse();
	public static final Response TERMINATE = new TerminateResponse();

	private static final Charset UTF8 = Charset.forName("UTF-8");
	/**
	 * Bute buffer, used for the request line and headers only.
	 */
	private ByteBuffer _buffer;

	/**
	 * Represents the byte stream state.
	 * 
	 * @author ieb
	 * 
	 */
	private enum State {
		/**
		 * Processing request line bytes
		 */
		IN_REQUEST_LINE,
		/**
		 * Processed a CR
		 */
		REQUEST_LINE_CR,
		/**
		 * In the request body
		 */
		IN_BODY, IN_ERROR
	}

	/**
	 * Represents the Header processing state.
	 * 
	 * @author ieb
	 * 
	 */
	private enum HeaderState {
		/**
		 * Processing the request line
		 */
		REQUEST_LINE,
		/**
		 * Processing headers
		 */
		IN_HEADERS

	}

	/**
	 * State for processing bytes, starts in the request line.
	 */
	private State state = State.IN_REQUEST_LINE;
	/**
	 * Header processing state, starting in the request line.
	 */
	private HeaderState headerState = HeaderState.REQUEST_LINE;
	/**
	 * The method that will process the result once headers are processed.
	 */
	private EventMethod method;
	/**
	 * A map of method factories. (shared amongst all requests, immutable)
	 */
	private MethodRegistry methodRegistry;
	/**
	 * Headers for this request
	 */
	private Map<String, List<String>> headers = new HashMap<String, List<String>>();
	/**
	 * Stores the current header, accumulating multiple lines as necessary.
	 */
	private StringBuilder currentHeader = new StringBuilder(255);
	/**
	 * Once the method is identified, the method factory will be created
	 */
	private ParsedRequestLine parsedRequestLine;
	private boolean checkedMethod = false;

	public RequestHandler(MethodRegistry methodRegistry, int buffersize) {
		this.methodRegistry = methodRegistry;
		this._buffer = ByteBuffer.allocateDirect(buffersize); // this buffer is
																// only used for
																// header
																// processing.
	}

	/**
	 * Update the request with new bytes, will return what the driver should do
	 * with the request.
	 * 
	 * @param buffer
	 *            the buffer containing bytes from the driver, already flipped
	 *            so that limit is the end of the buffer, and possiton is the
	 *            start.
	 * @return a Response object which will indicate what the driver needs to do
	 *         next.
	 * @throws HttpRequestException
	 * @throws IOException 
	 */
	public Response update(ByteBuffer buffer) throws HttpRequestException, IOException {
		try {
			if (state == State.IN_ERROR) {
				return TERMINATE;
			}
			if (state == State.IN_BODY) {
				return processBody(buffer);
			}

			for (int i = 0; i < buffer.limit(); i++) {
				byte c = buffer.get();
				switch (state) {
				case IN_REQUEST_LINE:
					if (!checkedMethod
							&& i > methodRegistry.getMaxMethodLength()) {
						// catch method errors early.
						throw new HttpRequestProtocolException(501);
					}
					if (c == '\r') {
						state = State.REQUEST_LINE_CR;
					} else if (!checkedMethod && c == ' ') {
						methodRegistry.get(getLineSegment());
						checkedMethod = true;
						_buffer.put(c);
					} else {
						_buffer.put(c);
					}
					break;
				case REQUEST_LINE_CR:
					if (c == '\n') {
						state = State.IN_REQUEST_LINE;
						Response response = processLine();
						if (response.end()) {
							return response;
						}
					} else {
						throw new HttpRequestProtocolException(400);
					}
					break;
				case IN_BODY:
					return processBody(buffer);
				case IN_ERROR:
					return TERMINATE; // not really required, but here for completeness.
				}
					
			}
			return CONTINUE;
		} catch (HttpRequestException e) {
			state = State.IN_ERROR;
			throw e;
		}
	}
	
	/**
	 * @return the headers as they are currently
	 */
	public Map<String, List<String>> getHeaders() {
		return headers;
	}
	/**
	 * @return the request line.
	 */
	public String getRequestLine() {
		return parsedRequestLine.getRequestLine();
	}
	
	public Response fillOutputBuffer(ByteBuffer buffer) throws IOException {
		return method.fillOutputBuffer(buffer);
	}

	public String getMethodName() {
		return parsedRequestLine.getMethod();
	}

	public String getPathInfo() {
		return parsedRequestLine.getPathInfo();
	}
	
	public String getProtocolVersion() {
		return parsedRequestLine.getProtocolVersion();
	}
	
	public String getQueryString() {
		return parsedRequestLine.getQueryString();
	}
	public Map<String, List<String>> getRequestParameters() {
		return parsedRequestLine.getParams();
	}



	/**
	 * Process the partial body of the request
	 * 
	 * @param buffer
	 * @param start2
	 * @param end
	 * @return
	 */
	private Response processBody(ByteBuffer buffer) {
		return method.processBody(buffer);
	}


	/**
	 * Process a line
	 * 
	 * @return
	 * @throws HttpRequestException 
	 * @throws IOException 
	 */
	private Response processLine() throws HttpRequestException, IOException {
		switch (headerState) {
		case REQUEST_LINE:
			parsedRequestLine = new ParsedRequestLine(getLine());
			MethodFactory methodFactory = methodRegistry.get(parsedRequestLine.getMethod());
			method = (EventMethod) methodFactory.create(this);
			headerState = HeaderState.IN_HEADERS;
			break;
		case IN_HEADERS:
			String line = getLine();
			if (line.length() == 0) {
				saveHeader("");
				state = State.IN_BODY;
				if ( method != null ) {
					return method.endOfHeaders();
				} else {
					// request was invalid, terminate and disconnect from the client.
					return TERMINATE;
				}
			}

			char fc = line.charAt(0);
			if (fc == ' ' || fc == '\t') {
				addToHeader(line);
			} else {
				saveHeader(line);
			}
			break;
		}
		return CONTINUE;
	}

	/**
	 * Add to the header
	 * 
	 * @param line
	 */
	private void addToHeader(String line) {
		currentHeader.append(line.trim());
	}

	/**
	 * Save and parse the header.
	 * 
	 * @param line
	 */
	private void saveHeader(String line) {
		addToHeader(line);
		if (currentHeader.length() == 0) {
			return;
		}
		String[] header = currentHeader.toString().split(":", 2);
		String k = header[0].trim();
		if (headers.containsKey(k)) {
			headers.get(k).add(header[1].trim());
		} else {
			ArrayList<String> l = new ArrayList<String>();
			l.add(header[1].trim());
			headers.put(k, l);
		}
		currentHeader.setLength(0);
	}
	

	/**
	 * Extract the line from the buffer.
	 * 
	 * @param pos2
	 * @return
	 */
	private String getLine() {
		_buffer.flip();
		byte[] b = new byte[_buffer.limit()];
		_buffer.get(b);
		_buffer.limit(_buffer.capacity());
		_buffer.rewind();
		String s = new String(b, UTF8);
		return s;
	}

	/**
	 * @return the line so far without resetting to a new line.
	 */
	private String getLineSegment() {
		byte[] b = new byte[_buffer.position()];
		int end = _buffer.position();
		int lim = _buffer.limit();
		_buffer.position(0);
		_buffer.limit(end);
		_buffer.get(b);
		_buffer.limit(lim);
		_buffer.position(end);
		String s = new String(b, UTF8);
		return s;
	}


	public boolean isHttp10() {
		return "1.0".equals(parsedRequestLine.getProtocolVersion());
	}

	public boolean shouldKeepAlive() {
		// TODO Auto-generated method stub
		return false;
	}

	public void checkKeepAlive(boolean expectsBody) {
		// TODO Auto-generated method stub
		
	}




}
