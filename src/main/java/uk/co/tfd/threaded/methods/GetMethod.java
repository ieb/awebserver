package uk.co.tfd.threaded.methods;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.Request;
import uk.co.tfd.threaded.OutputHandler;
import uk.co.tfd.threaded.RequestHandler;
import uk.co.tfd.threaded.ThreadedMethod;
import uk.co.tfd.threaded.View;
import uk.co.tfd.threaded.ViewResolver;

/**
 * Handles Get requests mapping to the file system.
 * @author ieb
 *
 */
public class GetMethod implements ThreadedMethod {

	private static final Logger LOG = LoggerFactory.getLogger(GetMethod.class);
	private Request request;
	private ViewResolver viewResolver;

	public GetMethod(Request request, ViewResolver viewResolver) {
		this.request = request;
		this.viewResolver = viewResolver;
	}

	public int processRequest(RequestHandler requestHandler,
			long endKeepAlive) throws IOException, HttpRequestException {
		View view = viewResolver.resolve(request);
		OutputHandler outputHandler = view.getOutputHander();
		outputHandler.output(requestHandler);
		return outputHandler.getStatusCode();
	}

}
