package uk.co.tfd;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Generic request inerface
 * 
 * @author ieb
 * 
 */
public interface Request {

	/**
	 * @return the raw request line.
	 */
	String getRequestLine();

	/**
	 * @return processed headers
	 */
	Map<String, List<String>> getHeaders();

	/**
	 * @return the method name parsed from the request line.
	 */
	String getMethodName();

	/**
	 * @return the path part of the URI
	 */
	String getPathInfo();

	/**
	 * @return parameters on the URI (not POST parameters)
	 * @throws IOException 
	 * @throws HttpRequestException 
	 */
	Map<String, List<String>> getRequestParameters() throws HttpRequestException, IOException;

	/**
	 * @return the raw qeury string.
	 */
	String getQueryString();

	/**
	 * @return the HTTP version as a string
	 */
	String getProtocolVersion();

	/**
	 * @return true if HTTP/1.0
	 */
	boolean isHttp10();

	/**
	 * @return true if the connection should be kept alive (persistent
	 *         connections)
	 */
	boolean shouldKeepAlive();

	/**
	 * re-evaluate keep alive with an indication of the body (content-length) of
	 * the request being relevant.
	 * 
	 * @param expectsBody true if the request body will be read, false if its assumed to be 0 length.
	 */
	void checkKeepAlive(boolean expectsBody);

}
