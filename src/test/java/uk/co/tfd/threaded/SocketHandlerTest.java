package uk.co.tfd.threaded;

import static org.junit.Assert.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Method;
import uk.co.tfd.MethodFactory;
import uk.co.tfd.MethodRegistry;
import uk.co.tfd.Request;

public class SocketHandlerTest {

	private static final Logger LOG = LoggerFactory.getLogger(SocketHandlerTest.class);
	
	
	class MockThreadedMethod implements ThreadedMethod {

		public int processRequest(RequestHandler requestHandler,
				long endKeepAlive) throws IOException, HttpRequestException {
			return 200;
		}



	}

	class MockMethodFactory implements MethodFactory {

		private String _method;
		private ThreadedMethod _eventMethod;

		public MockMethodFactory(String method, ThreadedMethod eventMethod) {
			this._method = method;
			this._eventMethod = eventMethod;
		}

		public Method create(Request request) throws HttpRequestProtocolException {
			if ( _method.equals(request.getMethodName())) {
				return _eventMethod;
			}
			throw new HttpRequestProtocolException(501);
		}

	}



	@Test
	public void testRequest() throws HttpRequestException, IOException {
		MethodRegistry methodRegistry = MethodRegistryLoader.getFactories();
		File f = new File("src/test/resources/testpatterns");
		for (File testf : f.listFiles()) {
			if ( testf.getName().startsWith(".")) {
				continue;
			}
			
			
			LOG.info("Test Pattern {} ", testf.getAbsolutePath());
			DataInputStream br = new DataInputStream(new FileInputStream(testf));
			final byte[] buffer = new byte[(int) testf.length()];
			br.readFully(buffer);
			br.close();
			final ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Socket socket = new Socket() {
				@Override
				public InputStream getInputStream() throws IOException {
					return  bais;
				}
				
				@Override
				public OutputStream getOutputStream() throws IOException {
					return baos;
				}
				
			};
			SocketHandler socketHandler = new SocketHandler(socket, methodRegistry, 1000, 100);
			socketHandler.processRequest(1000L);
		}
	}

	@Test
	public void testBadRequest() throws HttpRequestException, IOException {
		MethodRegistry methodRegistry = MethodRegistryLoader.getFactories();
		File f = new File("src/test/resources/badtestpatterns");
		for (File testf : f.listFiles()) {
			if ( testf.getName().startsWith(".")) {
				continue;
			}
			
			
			LOG.info("Test Pattern {} ", testf.getAbsolutePath());
			DataInputStream br = new DataInputStream(new FileInputStream(testf));
			final byte[] buffer = new byte[(int) testf.length()];
			br.readFully(buffer);
			br.close();
			final ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Socket socket = new Socket() {
				@Override
				public InputStream getInputStream() throws IOException {
					return  bais;
				}
				
				@Override
				public OutputStream getOutputStream() throws IOException {
					return baos;
				}
				
			};
			SocketHandler socketHandler = new SocketHandler(socket, methodRegistry, 1000, 100);
			try {
				socketHandler.processRequest(1000L);
				fail();
			} catch ( HttpRequestException e ) {
				LOG.error(e.getMessage());
				
			}
		}
	}

}
