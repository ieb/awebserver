package uk.co.tfd.threaded.responses;

import uk.co.tfd.Request;



/**
 * A bad Request response.
 * @author ieb
 *
 */
public class HttpBadRequestResponse extends  HttpResponse {

	public HttpBadRequestResponse(Request request) {
		super(request, 400);
	}

}
