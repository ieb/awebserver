package uk.co.tfd.threaded.resolvers;

import uk.co.tfd.Request;
import uk.co.tfd.threaded.View;
import uk.co.tfd.threaded.ViewResolver;
import uk.co.tfd.threaded.views.DefaultView;

public class DefaultViewResolver implements ViewResolver {

	public View resolve(Request request) {
		return new DefaultView(request);
	}

}
