package uk.co.tfd.threaded.methods;

import java.io.IOException;

import uk.co.tfd.Request;
import uk.co.tfd.threaded.RequestHandler;
import uk.co.tfd.threaded.ThreadedMethod;
import uk.co.tfd.threaded.responses.HttpMethodNotallowedResponse;

/**
 * Represents a method that has not been implemented.
 * 
 * @author ieb
 * 
 */
public class MethodNotImplemented implements ThreadedMethod {

	private Request request;

	public MethodNotImplemented(Request request) {
		this.request = request;
	}

	public int processRequest(RequestHandler socketHandler, long endKeepAlive)
			throws IOException {
		HttpMethodNotallowedResponse response = new HttpMethodNotallowedResponse(
				request, new String[] { "GET" });
		response.output(socketHandler);
		return response.getStatusCode();
	}

}
