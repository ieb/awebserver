package uk.co.tfd.event;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Method;
import uk.co.tfd.MethodFactory;
import uk.co.tfd.MethodRegistry;
import uk.co.tfd.Request;

public class RequestHanderTest {

	class MockEventMethod implements EventMethod {

		public Response processBody(ByteBuffer buffer) {

			return null;
		}

		public Response fillOutputBuffer(ByteBuffer buffer) {
			// TODO Auto-generated method stub
			return null;
		}

		public Response endOfHeaders() {
			return RequestHandler.CONTINUE;
		}

	}

	class MockMethodFactory implements MethodFactory {

		private String _method;
		private EventMethod _eventMethod;

		public MockMethodFactory(String method, EventMethod eventMethod) {
			this._method = method;
			this._eventMethod = eventMethod;
		}

		public Method create(Request request) throws HttpRequestProtocolException {
			if ( _method.equals(request.getMethodName())) {
				return _eventMethod;
			}
			throw new HttpRequestProtocolException(501);
		}

	}

	private static final Logger LOG = LoggerFactory
			.getLogger(RequestHanderTest.class);
	private MockEventMethod eventMethod;
	private MockMethodFactory methodFactory;
	private MethodRegistry methodRegistry;

	@Before
	public void before() {
		eventMethod = new MockEventMethod();
		methodFactory = new MockMethodFactory("GET", eventMethod);
		methodRegistry = new MethodRegistry();
		methodRegistry.put("GET", methodFactory);
	}

	@Test
	public void testRequest() throws HttpRequestException, IOException {
		// use a horribly small input buffer to simulate Slow Network IO.
		ByteBuffer inputbuffer = ByteBuffer.allocateDirect(10);
		File f = new File("src/test/resources/testpatterns");
		for (File testf : f.listFiles()) {
			if ( testf.getName().startsWith(".")) {
				continue;
			}
			RequestHandler requestHandler = new RequestHandler(methodRegistry,
					2912);
			LOG.info("Test Pattern {} ", testf.getAbsolutePath());
			DataInputStream br = new DataInputStream(new FileInputStream(testf));
			byte[] buffer = new byte[(int) testf.length()];
			br.readFully(buffer);
			br.close();
			int start = 0;
			int end = 0;
			Random r = new Random(0xdeadbeef);
			while (end < buffer.length) {
				int i = r.nextInt(inputbuffer.capacity());
				end = Math.min(start + i, buffer.length);
				LOG.info(
						"Performing put from {} to {} blen {} i {}  String[{}]",
						new Object[] { start, end, buffer.length, i,
								new String(buffer, start, end - start) });
				inputbuffer.put(buffer, start, end - start);
				inputbuffer.flip(); // now reading from buffer
				requestHandler.update(inputbuffer);
				inputbuffer.limit(inputbuffer.capacity());
				inputbuffer.rewind();
				start = end;
			}
			LOG.error("Request Line [{}] ", requestHandler.getRequestLine());
			LOG.error("Headers {} ", requestHandler.getHeaders());
		}
	}
	
	
	
}
