package uk.co.tfd.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpRequestException;

public class ParsedRequestLineTest {
	private static final String[] TESTS = new String[]{
		"GET http://www.xxx.com/solutions/mm-marketing/sdfsdfsd.html",
		"POST https://www.xxx.com/solutions/mm-marketing/sdfsdfsd.html HTTP/1.1",
		"GET https://www.xxx.com/solutions/mm-marketing/sdfsdfsd.html?x=1 HTTP/1.1",
		"PUT https://www.xxx.com:8080/solutions/mm-marketing/sdfsdfsd.html?x=1&y=5 HTTP/1.0",
		"GET https://www.xxx.com:8080/solutions/mm-marketing/sdfsdfsd.html?x=1&y=5=x=234?ASDAS HTTP/1.5",
		"GET /SDFSDFSFD/SDFSD?Q=21321"	
	};
	private static final boolean[] FAILED = {
		false,
		false,
		false,
		false,
		false,
		false,
	};
	private static final Logger LOG = LoggerFactory.getLogger(ParsedRequestLineTest.class);
	private static final String[] METHOD = new String[]{
		"GET",
		"POST",
		"GET",
		"PUT",
		"GET",
		"GET"
	};
	private static final String[] PATH_INFO = new String[]{
		"/solutions/mm-marketing/sdfsdfsd.html",
		"/solutions/mm-marketing/sdfsdfsd.html",
		"/solutions/mm-marketing/sdfsdfsd.html",
		"/solutions/mm-marketing/sdfsdfsd.html",
		"/solutions/mm-marketing/sdfsdfsd.html",
		"/SDFSDFSFD/SDFSD"	
	};
	private static final String[] PROTOCOL_VERSION = new String[]{
		"1.0",
		"1.1",
		"1.1",
		"1.0",
		"1.0",
		"1.0"	
	};
	private static final String[] QUERY_STRING = new String[]{
		"",
		"",
		"x=1",
		"x=1&y=5",
		"x=1&y=5=x=234?ASDAS",
		"Q=21321"	
		
	};
	private static final String[] URI = new String[]{
		"http://www.xxx.com/solutions/mm-marketing/sdfsdfsd.html",
		"https://www.xxx.com/solutions/mm-marketing/sdfsdfsd.html",
		"https://www.xxx.com/solutions/mm-marketing/sdfsdfsd.html?x=1",
		"https://www.xxx.com:8080/solutions/mm-marketing/sdfsdfsd.html?x=1&y=5",
		"https://www.xxx.com:8080/solutions/mm-marketing/sdfsdfsd.html?x=1&y=5=x=234?ASDAS",
		"/SDFSDFSFD/SDFSD?Q=21321"	
	};
	private static final Map<String,String[]>[] PARAMS = new HashMap[6];
	static {
		PARAMS[0] = new HashMap<String, String[]>();
		PARAMS[1] = new HashMap<String, String[]>();
		PARAMS[2] = new HashMap<String, String[]>();
		PARAMS[2].put("x", new String[]{"1"});
		PARAMS[3] = new HashMap<String, String[]>();
		PARAMS[3].put("x", new String[]{"1"});
		PARAMS[3].put("y", new String[]{"5"});
		PARAMS[4] = new HashMap<String, String[]>();
		PARAMS[4].put("x", new String[]{"1"});
		PARAMS[4].put("y", new String[]{"5=x=234?ASDAS"});
		PARAMS[5] = new HashMap<String, String[]>();
		PARAMS[5].put("Q", new String[]{"21321"});
	};


	@Test
	public void testPattern() {
		for (int i = 0; i < TESTS.length; i++ ) {
			ParsedRequestLine p;
			try {
				LOG.error("TEsting {} as {} ",i, TESTS[i]);
				p = new ParsedRequestLine(TESTS[i]);
				Assert.assertEquals(METHOD[i],p.getMethod());
				Assert.assertEquals(PATH_INFO[i],p.getPathInfo());
				Assert.assertEquals(PROTOCOL_VERSION[i],p.getProtocolVersion());
				Assert.assertEquals(QUERY_STRING[i],p.getQueryString());
				Assert.assertEquals(URI[i],p.getUri());
				Assert.assertEquals(PARAMS[i].size(),p.getParams().size());
				for ( Entry<String, List<String>> e : p.getParams().entrySet()) {
					Assert.assertArrayEquals(PARAMS[i].get(e.getKey()),e.getValue().toArray(new String[0]));
				}
				for ( Entry<String, String[]> e : PARAMS[i].entrySet()) {
					Assert.assertArrayEquals( p.getParams().get(e.getKey()).toArray(new String[0]),e.getValue());
				}
				LOG.info(p.toString());
			} catch (HttpRequestException e) {
				Assert.assertTrue(FAILED[i]);
			}
		}
	}

}
