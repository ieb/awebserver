package uk.co.tfd.event;

/**
 * This is a marker interface for all EventHandlers. EventHandlers receive
 * ByteBuffers as they become available, and and fill ByteBuffers when the Channel
 * they are connected to is ready to accept bytes. EventHandlers are attached to Channels 
 * that are registered with the Selector in the central event loop.
 * 
 * @author ieb
 * 
 */
public interface EventHandler {

}
