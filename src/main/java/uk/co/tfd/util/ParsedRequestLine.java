package uk.co.tfd.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.HttpRequestProtocolException;

import com.google.common.collect.Lists;

/**
 * Parses request lines.
 * @author ieb
 *
 */
public class ParsedRequestLine {
	private final static Pattern PATH_INFO_RE = Pattern
			.compile("\\S+:\\/\\/.*?(\\/.*)");
	private final static Pattern QUERY_STRING_RE = Pattern
			.compile("(.*?)\\?(.*)");
	String pathInfo;
	String queryString;
	Map<String, List<String>> params = new HashMap<String, List<String>>();
	private String stringView;
	private String method;
	private String uri;
	private String protocolVersion;
	private String requestLine;

	public ParsedRequestLine(String requestLine) throws HttpRequestException {
		this.requestLine = requestLine;
		// TODO: deal with %HEX encoding in the request line, spec 5.1.2
		String[] requestLineParts = requestLine.split(" ",3);
		if ( requestLineParts.length < 2) {
			throw new HttpRequestProtocolException(400); // spec 5.2 (or should it be a 501)
		}
		method = requestLineParts[0].trim();
		uri = requestLineParts[1].trim();
		if ( method.length() == 0 || uri.length() == 0 ) {
			throw new HttpRequestProtocolException(400);  // spec 5.2 (or should it be a 501)
		}
		// TODO: Make compliant wiht 3.1 of the spec.
		protocolVersion = "1.0";
		if ( requestLineParts.length == 3) {
			if ( "HTTP/1.1".equals(requestLineParts[2]) ) {
				protocolVersion = "1.1";
			}
		}
		queryString = "";
		pathInfo = uri;

		Matcher m = PATH_INFO_RE.matcher(uri);
		if (m.matches()) {
			pathInfo = m.group(1);
		}
		Matcher m2 = QUERY_STRING_RE.matcher(pathInfo);
		if (m2.matches()) {
			pathInfo = m2.group(1);
			queryString = m2.group(2);
		}
		queryStringToMap();
		stringView = toString();
	}
	
	public String getMethod() {
		return method;
	}
	public Map<String, List<String>> getParams() {
		return params;
	}
	public String getPathInfo() {
		return pathInfo;
	}
	/**
	 * @return the raw query string as it was in the URL, no sanitising.
	 */
	public String getQueryString() {
		return queryString;
	}
	public String getUri() {
		return uri;
	}
	public String getProtocolVersion() {
		return protocolVersion;
	}
	public String getRequestLine() {
		return requestLine;
	}


	private void queryStringToMap() {
		for (String param : queryString.split("&")) {
			if (param.length() == 0)
				continue;
			String[] parts = param.split("=", 2);
			if (parts.length == 1) {
				saveParam(parts[0], "");
			} else if (parts.length == 2) {
				saveParam(parts[0], parts[1]);
			}
		}
	}

	private void saveParam(String name, String value) {
		try {
			// assuming the query string was UTF8 encoded, because if it matters, it almost certainly was.
			name = URLDecoder.decode(name,"UTF-8");
			value = URLDecoder.decode(value,"UTF-8");
		} catch ( UnsupportedEncodingException e ) {
			// will never happen. Java always  has a UTF-8 Charset (famous last words).
		}
		List<String> values = params.get(name);

		if (values == null) {
			values = Lists.newArrayList();
			params.put(name, values);
		}
		values.add(value);
	}

	@Override
	public String toString() {
		if (stringView != null) {
			return stringView;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("PathInfo:").append(pathInfo).append("  Query String:")
				.append(queryString);
		for (Entry<String, List<String>> e : params.entrySet()) {
			sb.append(" Param:").append(e.getKey()).append(" Value:")
					.append(e.getValue());

		}
		return sb.toString();
	}


}
