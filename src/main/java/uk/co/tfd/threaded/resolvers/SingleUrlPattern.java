package uk.co.tfd.threaded.resolvers;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import uk.co.tfd.Request;
import uk.co.tfd.threaded.View;

import com.google.code.regexp.NamedMatcher;
import com.google.code.regexp.NamedPattern;

/**
 * Use regexes to match patterns to views.
 * NameMatcherTest shows that processing a URL through 100 regex matchers will add on average 18us to the
 * time taken to process the request, if the patterns have 2 named groups.
 * @author ieb
 *
 */
public class SingleUrlPattern implements UrlPattern {

	private NamedPattern pattern;
	private Constructor<View> constructor;
	
	public SingleUrlPattern(String patternString, String viewClassName) throws ClassNotFoundException, SecurityException, NoSuchMethodException {
		pattern = NamedPattern.compile(patternString);
		@SuppressWarnings("unchecked")
		Class<View> viewClass = (Class<View>) Class.forName(viewClassName);
		constructor = viewClass.getConstructor(Request.class, Map.class);
	}
	

	public uk.co.tfd.threaded.View createView(Request request) throws IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		NamedMatcher nm = pattern.matcher(request.getPathInfo());
		if ( nm.matches() ) {
			return constructor.newInstance(request, nm.namedGroups());
		}
		return null;
	}


	public void release() {
	}

}
