package uk.co.tfd.event.responses;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.co.tfd.Request;

public class DummyRequest implements Request {

	public String getRequestLine() {
		return null;
	}

	public Map<String, List<String>> getHeaders() {
		return new HashMap<String, List<String>>();
	}

	public String getMethodName() {
		return null;
	}

	public String getPathInfo() {
		return null;
	}

	public Map<String, List<String>> getRequestParameters() {
		return new HashMap<String, List<String>>();
	}

	public String getQueryString() {
		return null;
	}

	public String getProtocolVersion() {
		return null;
	}


	public boolean isHttp10() {
		return false;
	}

	public boolean shouldKeepAlive() {
		return true;
	}

	public void checkKeepAlive(boolean expectsBody) {
		// TODO Auto-generated method stub
		
	}

}
