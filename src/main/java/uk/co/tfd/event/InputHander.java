package uk.co.tfd.event;

import java.io.IOException;
import java.nio.ByteBuffer;

import uk.co.tfd.HttpRequestException;

/**
 * An input handler handles Input from the event loop. An example of an input
 * handler is the RequestHandler bound to the accepted http SocketChannel,
 * however if external resources are used from which input may need to be read
 * then an input handler will be attached to the channel where the bytes from
 * that external source will come.
 * 
 * @author ieb
 * 
 */
public interface InputHander extends EventHandler {

	/**
	 * 
	 * @param buffer
	 * @return
	 * @throws HttpRequestException
	 * @throws IOException 
	 */
	Response update(ByteBuffer buffer) throws HttpRequestException, IOException;

}
