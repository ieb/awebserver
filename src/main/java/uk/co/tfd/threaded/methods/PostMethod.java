package uk.co.tfd.threaded.methods;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.Request;
import uk.co.tfd.threaded.OutputHandler;
import uk.co.tfd.threaded.RequestHandler;
import uk.co.tfd.threaded.ThreadedMethod;
import uk.co.tfd.threaded.responses.HttpBadRequestResponse;
import uk.co.tfd.threaded.responses.HttpContinueResponse;
import uk.co.tfd.threaded.responses.HttpPOSTRedirectResponse;

/**
 * Handles a POST method, by printing the post out to the logs. Doesnt do
 * anything more useful.
 * 
 * @author ieb
 * 
 */
public class PostMethod implements ThreadedMethod {

	private static final Logger LOG = LoggerFactory.getLogger(PostMethod.class);
	private Request request;

	public PostMethod(Request request) {
		this.request = request;
	}

	public int processRequest(RequestHandler requestHandler, long endKeepAlive)
			throws IOException, HttpRequestException {

		Map<String, List<String>> headers = request.getHeaders();
		Form form = new Form(request, requestHandler); // this could be resolved
		if ( ! form.isStandardForm() ) {
			// check the headers are Ok and we can read the post.
			OutputHandler o = new HttpBadRequestResponse(request);
			o.output(requestHandler);
			return o.getStatusCode(); 
		}
		// did we get an Expect: 100-continue, if we did send a 100 Continue
		if ( (!request.isHttp10()) && headers.containsKey("Expect") && "100-continue".equals(headers.get("Expect").get(0)) ) {
			
			OutputHandler o = checkRequest();
			o.output(requestHandler);
			requestHandler.flush();
		}

		OutputHandler outputHandler = null;
		if (!form.isStandardForm()) {
			outputHandler = new HttpBadRequestResponse(request);
		} else {
			Map<String, List<String>> formParams = form.getParams();
			LOG.error("Got Post containing {} ", formParams);
			outputHandler = new HttpPOSTRedirectResponse(request, request
					.getHeaders().get("Referer").get(0));
		}

		outputHandler.output(requestHandler);
		return outputHandler.getStatusCode();
	}

	private OutputHandler checkRequest() {
		// put any other request checking code here, and if a problem is encoundered respond with the appropriate code
		return  new HttpContinueResponse(request);
	}

}
