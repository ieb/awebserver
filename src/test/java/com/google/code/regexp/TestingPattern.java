package com.google.code.regexp;

import java.util.List;

import org.junit.Assert;

public class TestingPattern {
	private String pattern;
	private List<String> groupNames;
	private List<NamedMatch> testPatterns;
	private List<String> badTestPatterns;
	private NamedPattern namedPattern;

	public TestingPattern(String pattern, List<String> groupNames,
			List<NamedMatch> testPatterns, List<String> badTestPatterns) {
		this.pattern = pattern;
		this.groupNames = groupNames;
		this.testPatterns = testPatterns;
		this.badTestPatterns = badTestPatterns;
	}
	
	public void check() {
		this.namedPattern = NamedPattern.compile(pattern);
		Assert.assertArrayEquals(namedPattern.groupNames().toArray(), groupNames.toArray());
		
		for ( NamedMatch m : testPatterns ) {
			m.check(namedPattern);
		}
		
		for ( String p : badTestPatterns ) {
			NamedMatcher matcher = namedPattern.matcher(p);
			Assert.assertFalse(matcher.matches());
		}
	}
}
