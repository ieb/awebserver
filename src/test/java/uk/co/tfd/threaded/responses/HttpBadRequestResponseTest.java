package uk.co.tfd.threaded.responses;

import java.io.IOException;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpRequestException;

public class HttpBadRequestResponseTest extends AbsrtractResponseTest {

	private static final Logger LOG = LoggerFactory.getLogger(HttpBadRequestResponseTest.class);

	@Test
	public void test() throws IOException, HttpRequestException {
		HttpBadRequestResponse response = new HttpBadRequestResponse(getRequest());
		String responseString = responseTest(response);
		LOG.error("Response is {} ",responseString);
		checkResponseString(responseString,400);
	}

}
