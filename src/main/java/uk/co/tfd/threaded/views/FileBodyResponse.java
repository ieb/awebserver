package uk.co.tfd.threaded.views;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.Request;
import uk.co.tfd.ResponseHeaders;
import uk.co.tfd.threaded.OutputHandler;
import uk.co.tfd.threaded.RequestHandler;
import uk.co.tfd.threaded.View;
import uk.co.tfd.threaded.responses.HttpNotModifiedResponse;
import uk.co.tfd.threaded.responses.HttpResponse;
import uk.co.tfd.util.ContentType;
import uk.co.tfd.util.HttpDate;

/**
 * Generates a File Body response conforming to If-Modified-Since request
 * headers if present.
 * 
 * @author ieb
 * 
 */
public class FileBodyResponse implements OutputHandler, View {

	private static final Logger LOG = LoggerFactory
			.getLogger(FileBodyResponse.class);
	private HttpResponse responseHeaders;
	private File requestedFile;
	private boolean no_body;
	private Request request;

	/**
	 * Construct a FileBody response from the request, given a File. This will
	 * perform processing upto the first byte of the body of the response and
	 * prepare for that.
	 * 
	 * @param request
	 * @param requestedFile
	 * @param head 
	 * @throws IOException
	 */
	public FileBodyResponse(Request request, final File requestedFile)
			throws IOException {
		this.request = request;
		this.requestedFile = requestedFile;
		if (notModified()) {
			responseHeaders = new HttpNotModifiedResponse(request);
			no_body = true;
			return;
		}
		long toread = requestedFile.length();
		ResponseHeaders h = new ResponseHeaders();
		h.setDate("Last-Modified", requestedFile.lastModified());
		// the spec 9.4 says "The metainformation contained in the HTTP headers in
		// response to a HEAD request SHOULD be identical to the information sent in
		// response to a GET request. "
		// So we must set the Content-Length.
		// Clients reading having sent the HEAD request should not attempt to read the body.
		// libcurl is broken when invoked with curl -X HEAD ...
		h.set("Content-Length", toread);
		h.set("Content-Type", ContentType.toString(requestedFile));
		responseHeaders = new HttpResponse(request, 200, h);
		no_body = "HEAD".equals(request.getMethodName());
	}

	/**
	 * At the moment this just uses If-Modified-Since which appears to work well
	 * with Chrome.
	 * 
	 * @return true if the file body has not been modified based on the headers.
	 */
	private boolean notModified() {
		Map<String, List<String>> h = request.getHeaders();
		if (h.containsKey("If-Modified-Since")) {
			Date ifmodified = HttpDate.parse(h.get("If-Modified-Since").get(0));
			if (requestedFile.lastModified() <= ifmodified.getTime()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Send the response, first the headers then the body of the file.
	 * 
	 * @see uk.co.tfd.threaded.OutputHandler#output(uk.co.tfd.threaded.RequestHandler)
	 */
	public void output(RequestHandler requestHandler) throws IOException {
		responseHeaders.output(requestHandler);
		if (no_body)
			return;
		FileInputStream fileIn = new FileInputStream(requestedFile);
		int n = 0;
		try {
			byte[] buffer = new byte[2048];
			while (true) {
				int i = fileIn.read(buffer);
				if (i < 0)
					break;
				requestHandler.writeByte(buffer, 0, i);
				n = n + i;
			}
		} catch ( IOException e) {
			LOG.error("Failed {} ",e.getMessage(),e);
			throw e;
		} finally {
			LOG.debug("Finished Writing Body, wrote {} bytes", n);
			try {
				fileIn.close();
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

	/**
	 * Get the status code that was sent with the response.
	 * 
	 * @see uk.co.tfd.threaded.OutputHandler#getStatusCode()
	 */
	public int getStatusCode() {
		return responseHeaders.getStatusCode();
	}

	public OutputHandler getOutputHander() {
		return this;
	}

}
