package uk.co.tfd.threaded.responses;

import java.io.IOException;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpRequestException;

public class HttpRedirectResponseTest extends AbsrtractResponseTest {

	private static final Logger LOG = LoggerFactory.getLogger(HttpRedirectResponseTest.class);

	@Test
	public void testPOST() throws IOException, HttpRequestException {
		HttpPOSTRedirectResponse response = new HttpPOSTRedirectResponse(getRequest(), "/somewhereelse.html");
		String responseString = responseTest(response);
		LOG.error("Response is {} ",responseString);
		checkResponseString(responseString,303);
	}

	@Test
	public void testTemp() throws IOException, HttpRequestException {
		HttpTempRedirectResponse response = new HttpTempRedirectResponse(getRequest(), "/somewhereelse.html");
		String responseString = responseTest(response);
		LOG.error("Response is {} ",responseString);
		checkResponseString(responseString,302);
	}

	@Test
	public void testPerm() throws IOException, HttpRequestException {
		HttpPermRedirectResponse response = new HttpPermRedirectResponse(getRequest(), "/somewhereelse.html");
		String responseString = responseTest(response);
		LOG.error("Response is {} ",responseString);
		checkResponseString(responseString,301);
	}

}
