package com.google.code.regexp;
/**
 * Taken from http://code.google.com/p/named-regexp/
 * Licensed under ASL2
 * @author clement.denis
 * No maven repo release available.
 * Used while not on Java 7 which has named regex support.
 * 
 * Rewritten as the tests didnt work very well.
 */
import java.util.List;
import java.util.Map;
import java.util.Random;

import junit.framework.Assert;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

public class NamedMatcherTest {

    private static final Logger LOG = LoggerFactory.getLogger(NamedMatcherTest.class);

    @Test
    public void test() {
    	for ( TestingPattern tp : Patterns.patterns) {
    		tp.check();
        }
    }
    
    
    @Test
    public void urlTimingTest() {
    	NamedPattern np = NamedPattern.compile("/testing/(?<part1>.*?)/(?<part2>.*)/");
    	List<String[]> urls = Lists.newArrayList();
    	Random r = new Random(0xdeadbeef);
    	for ( int i = 0; i < 1000; i++ ) {
    		String p1 = String.valueOf(r.nextLong());
    		String p2 = String.valueOf(r.nextLong());
    		urls.add(new String[] {
    				"/testing/"+p1+"/"+p2+"/",
    				p1,
    				p2
    		});
    	}
    	for ( String[] url : urls ) {
    		NamedMatcher nm = np.matcher(url[0]);
    		Assert.assertTrue(nm.matches());
    		Map<String, String> groups = nm.namedGroups();
    		Assert.assertEquals(2, groups.size());
    		Assert.assertEquals(groups.get("part1"), url[1]);
    		Assert.assertEquals(groups.get("part2"), url[2]);
    	}
    	long start = System.nanoTime();
    	for ( int i = 0; i < 100; i++ ) {
	    	for ( String[] url : urls ) {
	    		@SuppressWarnings("unused")
				String s = url[0];
	    	}
    	}
    	long overhead = System.nanoTime() - start;
    	start = System.nanoTime();
    	for ( int i = 0; i < 100; i++ ) {
	    	for ( String[] url : urls ) {
	    		NamedMatcher nm = np.matcher(url[0]);
	    		nm.matches();
	    		@SuppressWarnings("unused")
				Map<String, String> groups = nm.namedGroups();
	    	}
    	}
    	long parsing = System.nanoTime() - start - overhead;
    	LOG.info("Parsed {} urls in {} ",(100*urls.size()),parsing);
    	LOG.info("Time per URL is {}E-9s ",parsing/(100*urls.size()));
    	LOG.info("Overhead for 100 regexes in {} ms ", (double)(100*parsing/(100*urls.size()))/(1000000));
    }
    
    @Test
    public void urlTimingTestOneMatch() {
    	List<NamedPattern> patterns = Lists.newArrayList();
    	Random r = new Random(0xdeadbeef);
    	// set up the patterns
    	for ( int i = 0; i < 50; i++ ) {
    		String p1 = String.valueOf(r.nextLong());
        	NamedPattern np = NamedPattern.compile("/testing-"+p1+"/(?<part1>.*?)/(?<part2>.*)/");
        	patterns.add(np);
    	}
    	// make the last one a match, normal distribution
    	patterns.add(NamedPattern.compile("/testing/(?<part1>.*?)/(?<part2>.*)/"));

    	// setup the urls to match the last one.
    	List<String[]> urls = Lists.newArrayList();
    	for ( int i = 0; i < 1000; i++ ) {
    		String p1 = String.valueOf(r.nextLong());
    		String p2 = String.valueOf(r.nextLong());
    		urls.add(new String[] {
    				"/testing/"+p1+"/"+p2+"/",
    				p1,
    				p2
    		});
    	}
    	// make sure every url does match.
    	for ( String[] url : urls ) {
    		boolean matched = false;
    		for ( NamedPattern np : patterns) {
	    		NamedMatcher nm = np.matcher(url[0]);
	    		if (nm.matches()) {
		    		Map<String, String> groups = nm.namedGroups();
		    		Assert.assertEquals(2, groups.size());
		    		Assert.assertEquals(groups.get("part1"), url[1]);
		    		Assert.assertEquals(groups.get("part2"), url[2]);
		    		matched = true;
	    		}
    		}
    		Assert.assertTrue(matched);
    	}
    	long start = System.nanoTime();
    	for ( String[] url : urls ) {
    		for ( @SuppressWarnings("unused") NamedPattern np : patterns) {
	    		@SuppressWarnings("unused")
				String s = url[0];
	    	}
    	}
    	long overhead = System.nanoTime() - start;
    	start = System.nanoTime();
    	for ( String[] url : urls ) {
    		for ( NamedPattern np : patterns) {
	    		NamedMatcher nm = np.matcher(url[0]);
	    		if (nm.matches() ) {
	    			@SuppressWarnings("unused")
					Map<String, String> groups = nm.namedGroups();
	    		}
	    	}
    	}
    	long parsing = System.nanoTime() - start - overhead;
    	LOG.info("Parsed {} urls in {} ",(urls.size()),parsing);
    	LOG.info("Average parsing time for 100 regex patterns {} ms ", (double)(parsing/(urls.size()))/(1000000));
    }

}