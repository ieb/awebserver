package uk.co.tfd.event;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * Performs event dispatch by opening a server socket, attaching it to selector and then starting 0-n threads
 * which select on that selector for events. This class itself starts a worker and goes into an event loop.
 * To stop, the stop method is called and all EventDispatcherWorkers are stopped.
 * @author ieb
 *
 */
public class MultiThreadedEventDispatcher implements EventDispatcher {

	private int port;
	private int backlog;
	private boolean running;
	private Selector selector;
	private List<EventDispatcherWorker> dispatchers = new ArrayList<EventDispatcherWorker>();
	private int nthreads;
	
	public MultiThreadedEventDispatcher(int nthreads) {
		this.nthreads = nthreads;
	}

	public void start() throws IOException {
		if ( running ) {
			throw new IllegalStateException("Already Runinng, stop first please.");
		}
		// Create a non blocking socket start to listen
		// Go into an event loop
		// as eash event comes out dispatch to the appropriate request object.
		ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
		serverSocketChannel.configureBlocking(false);
		serverSocketChannel.socket().bind(new InetSocketAddress(port), backlog);
		selector = Selector.open();
		serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
		running = true;
		
		// the selector is thread safe, so we can spin up multiple workers and bind to the selector.
		for ( int i = 0; i < nthreads-1; i++ ) {
			EventDispatcherWorker worker = new EventDispatcherWorker(selector);
			dispatchers.add(worker);
			Thread t = new Thread(worker);
			t.start();
		}
		EventDispatcherWorker finalWorker = new EventDispatcherWorker(selector);
		dispatchers.add(finalWorker);
		finalWorker.run();
	}
	
	public SelectionKey register(SelectableChannel channel, int ops, Object attachment) throws ClosedChannelException {
		return channel.register(selector, ops, attachment);
	}



	public void stop() {
		for (EventDispatcherWorker dw : dispatchers) {
			dw.stop();
		}
		running = false;
	}
}
