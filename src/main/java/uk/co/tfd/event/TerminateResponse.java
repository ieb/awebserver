package uk.co.tfd.event;

/**
 * Terminate the event source.
 * @author ieb
 *
 */
public class TerminateResponse extends EmptyResponse {

	public boolean end() {
		return true;
	}

}
