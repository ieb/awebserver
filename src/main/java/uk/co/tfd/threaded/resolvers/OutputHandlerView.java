package uk.co.tfd.threaded.resolvers;

import uk.co.tfd.threaded.OutputHandler;
import uk.co.tfd.threaded.View;

public class OutputHandlerView implements View {

	private OutputHandler outputHandler;

	public OutputHandlerView(OutputHandler outputHandler) {
		this.outputHandler = outputHandler;
	}

	public OutputHandler getOutputHander() {
		return outputHandler;
	}

}
