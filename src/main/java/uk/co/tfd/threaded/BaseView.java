package uk.co.tfd.threaded;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import uk.co.tfd.threaded.methods.Form;
import uk.co.tfd.threaded.responses.HttpBadRequestResponse;
import uk.co.tfd.threaded.responses.HttpContinueResponse;
import uk.co.tfd.threaded.responses.HttpMethodNotallowedResponse;
import uk.co.tfd.threaded.responses.HttpServerErrorResponse;

import com.google.common.collect.Lists;

/**
 * A base view to handle all methods.
 * @author ieb
 *
 */
public abstract class BaseView implements View, OutputHandler {
	
	private static final String[] EXPECTED_METHODS = { "GET", "HEAD", "POST" };
	protected ThreadedRequest request;
	protected Map<String, String> parameters;
	private OutputHandler outputHandler;
	private String[] methods;
	private Method method;
	private int statusCode;
	
	public BaseView(ThreadedRequest request, Map<String, String> parameters) {
		this.request = request;
		this.parameters = parameters;
		method = getInvoker(request.getMethodName());
		if ( method == null ) {
			outputHandler = new HttpMethodNotallowedResponse(request, getMethods());
		}
		if ("POST".equals(request.getMethodName())) {
			Form form = request.getForm();
			if (!form.isStandardForm()) {
				// check the headers are Ok and we can read the post.
				outputHandler = new HttpBadRequestResponse(request);
			}

		}
	}


	/**
	 * @param method the method that the http request maps to. The method must be void and start with do.
	 * eg GET goes to doGet()
	 * @return
	 */
	private Method getInvoker(String method) {
		try {
			return this.getClass().getMethod(getMethodName(method), RequestHandler.class);
		} catch (SecurityException e) {
			return null;
		} catch (NoSuchMethodException e) {
			return null;
		}
	}



	/**
	 * @param method
	 * @return the java method name to handle the request.
	 */
	private String getMethodName(String method) {
		return "do"+method.substring(0,1).toUpperCase()+method.substring(1).toLowerCase();
	}



	/**
	 * @return a cached list of methods
	 */
	private String[] getMethods() {
		if (methods == null) {
			List<String> methodsList = Lists.newArrayList();
			for ( String m : getExpectedMethods() ) {
				try {
						methodsList.add(m);
					
				} catch ( Exception e ) {		
				}
			}
			methods = methodsList.toArray(new String[methodsList.size()]);
		}
		return methods;
	}




	/**
	 * Override this if you want to add methods not in the normal list. Its used to introspect the methods
	 * implemented for a 405 response. No other reason.
	 * @return list of expected methods the view will implement
	 */
	protected String[] getExpectedMethods() {
		return EXPECTED_METHODS;
	}



	/**
	 * This is final so that we can guarantee that he outputHandler will be right
	 * if there is an error. Subclasses should implement getViewOutputHandler()
	 * @see uk.co.tfd.threaded.View#getOutputHander()
	 */
	public final OutputHandler getOutputHander() {
		if ( outputHandler != null ) {
			return outputHandler;
		}
		return this;
	}
	
	public int getStatusCode() {
		if ( outputHandler != null ) {
			return outputHandler.getStatusCode();
		}
		return statusCode;
	}
	
	public void output(RequestHandler requestHandler) throws IOException {
		
		try {
			Map<String, List<String>> headers = request.getHeaders();
			// did we get an Expect: 100-continue, if we did send a 100 Continue
			if ("POST".equals(request.getMethodName())) {
				if ((!request.isHttp10()) && headers.containsKey("Expect")
						&& "100-continue".equals(headers.get("Expect").get(0))) {
					OutputHandler o = checkRequest();
					o.output(requestHandler);
					requestHandler.flush();
					if ( ! ( o instanceof HttpContinueResponse )) {
						statusCode = o.getStatusCode();
						return; // there was an error, dont to any more.
					}
				}
			}
			statusCode = (Integer) method.invoke(this, requestHandler);
		} catch (Exception e) {
			new HttpServerErrorResponse(request, e).output(requestHandler);
		}
	}

	/**
	 * @return a continue response if the request headers are Ok and the a
	 *         100-Continue can be sent back to the client, otherwise a suitable
	 *         response.
	 */
	private OutputHandler checkRequest() {
		// put any other request checking code here, and if a problem is
		// encountered respond with the appropriate code
		return new HttpContinueResponse(request);
	}

}
