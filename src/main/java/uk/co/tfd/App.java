package uk.co.tfd;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.threaded.ThreadedDispatcher;

/**
 * The main app. Starts a threaded web server dispatch loop.
 * 
 */
public class App {
	private ThreadedDispatcher dispatcher;
	private static final Logger LOG = LoggerFactory.getLogger(App.class);

	public App(String[] args) {
		int threads = Integer.parseInt(args[0]);
		int port = Integer.parseInt(args[1]);
		long keepAliveTTL = Long.parseLong(args[2]);
		int maxRequests = Integer.parseInt(args[3]);
		dispatcher = new ThreadedDispatcher(threads, port, keepAliveTTL,
				maxRequests);
	}

	private void run() throws IOException, InterruptedException {
		dispatcher.start();
	}

	protected void stop() {
		dispatcher.stop();
	}

	public static void main(String[] args) throws IOException,
			InterruptedException {
		final App a = new App(new String[] { "100", "9000", "5000", "10" });
		a.run();
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				a.stop();
				LOG.info("Stopped Server");
			}
		});

	}

}
