package uk.co.tfd.threaded.methods;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.event.responses.DummyRequest;
import uk.co.tfd.threaded.RequestHandler;

public class FormTest {

	private static final Logger LOG = LoggerFactory.getLogger(FormTest.class);

	@Test
	public void test() throws IOException, HttpRequestException {
		final Map<String, List<String>> headers = new HashMap<String, List<String>>();
		List<String> v = new ArrayList<String>();
		v.add("application/x-www-form-urlencoded; charset=utf-8");
		headers.put("Content-Type", v);
		DummyRequest request = new DummyRequest() {
			@Override
			public Map<String, List<String>> getHeaders() {
				return headers;
			}

			@Override
			public String getMethodName() {
				return "POST";
			}
		};

		File f = new File("src/test/resources/testpostpatterns");
		for (File testf : f.listFiles()) {
			if (testf.getName().startsWith(".")) {
				continue;
			}
			LOG.info("Test Pattern {} ", testf.getAbsolutePath());
			final FileInputStream in = new FileInputStream(testf);

			RequestHandler requestHandler = new RequestHandler() {

				public void writeByte(byte[] buffer, int off, int len)
						throws IOException {
					fail("Not expecting writes for this test");
				}

				public String readLine(int spaceBefore) throws IOException,
						HttpRequestException {
					fail("Not expecting read lines");
					return "";
				}

				public int readByte(byte[] buffer, int off, int len)
						throws IOException {
					return in.read(buffer, off, len);
				}

				public void flush() throws IOException {
					
				}
			};
			Form form = new Form(request, requestHandler);
			Assert.assertTrue(form.isStandardForm());
			Assert.assertTrue(form.getParams().size() > 0);
			LOG.error("Form is {} ",form.getParams());
			in.close();
		}
	}
}
