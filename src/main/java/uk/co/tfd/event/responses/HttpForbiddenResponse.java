package uk.co.tfd.event.responses;



public class HttpForbiddenResponse extends  HttpResponse {

	public HttpForbiddenResponse() {
		super(403);
	}

}
