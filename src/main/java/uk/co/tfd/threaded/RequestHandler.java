package uk.co.tfd.threaded;

import java.io.IOException;

import uk.co.tfd.HttpRequestException;

/**
 * Handles IO on the request part of a HTTP message.
 * 
 * @author ieb
 * 
 */
public interface RequestHandler {

	/**
	 * Read a line in the request line or header section of the request. Should
	 * not be called once into the body of the request due to charset encoding.
	 * 
	 * @param spaceBefore
	 *            how many bytes to read before a space must appear. If <= 0,
	 *            then it will be ignored. This helps protect from rogue clients
	 *            to trying play with the request method.
	 * @return the line with CRLF stripped.
	 * @throws IOException
	 *             if there is a problem reading.
	 * @throws HttpRequestException
	 *             If there is a HTTP protocol problem with the line.
	 */
	String readLine(int spaceBefore) throws IOException, HttpRequestException;

	/**
	 * Read bytes from the body. All headers must have been read before this is
	 * called.
	 * 
	 * @param buffer
	 *            the buffer to read into
	 * @param off
	 *            starting offset
	 * @param len
	 *            the max number of bytes to read.
	 * @return the number of bytes read or -1 if an end of stream or end of body
	 *         was encountered.
	 * @throws IOException
	 *             if there was a problem with the stream.
	 */
	int readByte(byte[] buffer, int off, int len) throws IOException;

	/**
	 * Write bytes to the client.
	 * 
	 * @param buffer
	 *            the buffer to write.
	 * @param off
	 *            offset into the buffer
	 * @param len
	 *            number of bytes to write.
	 * @throws IOException
	 *             if there was a problem writing to the client.
	 */
	void writeByte(byte[] buffer, int off, int len) throws IOException;

	/**
	 * Forces a flush of the output stream
	 * @throws IOException 
	 */
	void flush() throws IOException;
}
