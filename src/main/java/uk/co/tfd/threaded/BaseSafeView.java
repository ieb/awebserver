package uk.co.tfd.threaded;

import java.io.IOException;
import java.util.Map;

import uk.co.tfd.threaded.responses.HttpMethodNotallowedResponse;

/**
 * A base view for non modifying views.
 * 
 * @author ieb
 * 
 */
public abstract class BaseSafeView extends BaseView {

	private static final String[] EXPECTED_METHODS = { "GET", "HEAD" };

	public BaseSafeView(ThreadedRequest request, Map<String, String> parameters) {
		super(request, parameters);
	}

	public final int doPost(RequestHandler requestHandler) throws IOException {
		return denied(requestHandler);
	}
	public final int doPut(RequestHandler requestHandler) throws IOException {
		return denied(requestHandler);
	}
	public final int doDelete(RequestHandler requestHandler) throws IOException {
		return denied(requestHandler);
	}

	private int denied(RequestHandler requestHandler) throws IOException {
		OutputHandler oh = new HttpMethodNotallowedResponse(request,
				EXPECTED_METHODS);
		oh.output(requestHandler);
		return oh.getStatusCode();
	}

	/**
	 * Override this if you want to add methods not in the normal list. Its used
	 * to introspect the methods implemented for a 405 response. No other
	 * reason.
	 * 
	 * @return list of expected methods the view will implement
	 */
	@Override
	protected String[] getExpectedMethods() {
		return EXPECTED_METHODS;
	}

}
