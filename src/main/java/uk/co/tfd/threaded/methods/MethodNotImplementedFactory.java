package uk.co.tfd.threaded.methods;

import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Method;
import uk.co.tfd.MethodFactory;
import uk.co.tfd.Request;

/**
 * Base class for all method factories with no impl.
 * @author ieb
 *
 */
public class MethodNotImplementedFactory implements MethodFactory {

	/** 
	 * Simply creates a method not implemented response.
	 * @see uk.co.tfd.MethodFactory#create(uk.co.tfd.Request)
	 */
	public Method create(Request request) throws HttpRequestProtocolException {
		return new MethodNotImplemented(request);
	}

}
