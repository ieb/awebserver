package uk.co.tfd.event;

import java.io.IOException;
import java.nio.ByteBuffer;

import uk.co.tfd.Method;

/**
 * Represents a method that can accept a request
 * 
 * @author ieb
 * 
 */
public interface EventMethod extends Method {
	/**
	 * Processes the body of a request.
	 * 
	 * @param buffer
	 *            the buffer that is the body
	 * @return a Response object that may request more input or might register
	 *         a selectable channel for output with the event loop.
	 */
	Response processBody(ByteBuffer buffer);

	/**
	 * Called when the headers are complete
	 * @return 
	 * @throws IOException 
	 */
	Response endOfHeaders() throws IOException;

	/**
	 * Called to fill the buffer for writing.
	 * @param buffer
	 * @return
	 * @throws IOException 
	 */
	Response fillOutputBuffer(ByteBuffer buffer) throws IOException;
}
