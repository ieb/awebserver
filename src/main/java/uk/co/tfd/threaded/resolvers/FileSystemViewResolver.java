package uk.co.tfd.threaded.resolvers;

import java.io.File;
import java.io.IOException;

import uk.co.tfd.Request;
import uk.co.tfd.threaded.View;
import uk.co.tfd.threaded.ViewResolver;
import uk.co.tfd.threaded.responses.HttpNotFoundResponse;
import uk.co.tfd.threaded.views.DirectoryResponse;
import uk.co.tfd.threaded.views.FileBodyResponse;

public class FileSystemViewResolver implements ViewResolver {

	private File webrootFile;

	public FileSystemViewResolver(String webroot) {
		this.webrootFile = new File(webroot);
	}

	public View resolve(Request request) throws IOException {
		File requestedFile = new File(webrootFile,request.getPathInfo());

		if ( requestedFile.getAbsolutePath().startsWith(webrootFile.getAbsolutePath())) {
			if ( requestedFile.isFile() ) {
				return new FileBodyResponse(request, requestedFile);
			} else if ( requestedFile.isDirectory() ) {
				return new DirectoryResponse(request, requestedFile);
			} else {
				return new OutputHandlerView(new HttpNotFoundResponse(request));
			}
		}
		return null;
	}

}
