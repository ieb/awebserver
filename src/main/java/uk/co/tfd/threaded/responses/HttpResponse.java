package uk.co.tfd.threaded.responses;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.co.tfd.Request;
import uk.co.tfd.ResponseHeaders;
import uk.co.tfd.Server;
import uk.co.tfd.threaded.OutputHandler;
import uk.co.tfd.threaded.RequestHandler;
import uk.co.tfd.util.CharSet;

/**
 * Base HttpResponse class that collects and outputs headers upto the body part
 * of the response.
 * 
 * @author ieb
 * 
 */
public class HttpResponse implements OutputHandler {

	/**
	 * Map of acceptable HTTP 1.0 Status codes, loaded from a properties file.
	 */
	private static final Map<Integer, String> STATUS_CODES_10 = new HashMap<Integer, String>();
	/**
	 * Map of acceptable HTTP 1.1 Status codes, loaded from a properties file.
	 */
	private static final Map<Integer, String> STATUS_CODES_11 = new HashMap<Integer, String>();
	private static final Logger LOG = LoggerFactory
			.getLogger(HttpResponse.class);
	static {
		{
			loadCodes("http10codes.properties", STATUS_CODES_10);
			loadCodes("http11codes.properties", STATUS_CODES_11);
		}
	}

	/**
	 * A byte[] representing the headers ready for dumping to the output stream.
	 */
	private byte[] headers;
	/**
	 * Status code of the response
	 */
	private int statusCode;

	/**
	 * Create and commit the response. Once this is invoked the response is
	 * ready to commit. It wont be committed until the response is output. You
	 * can create more than one response from the same request, although that
	 * would be wastefull.
	 * 
	 * @param request
	 *            the request which the response is a response to.
	 * @param statusCode
	 *            the status code.
	 * @param h
	 *            headers to go out in the response
	 */
	public HttpResponse(Request request, int statusCode, ResponseHeaders h) {
		this.statusCode = statusCode;
		StringBuilder sb = new StringBuilder();
		String scm = STATUS_CODES_11.get(statusCode);
		if (request.isHttp10()) {
			statusCode = convertToHttp10(statusCode);
			scm = STATUS_CODES_10.get(statusCode);

		}
		if (scm == null) {
			scm = "HTTP/1.1 " + statusCode + " UNDEFINED\r\n";
		}
		sb.append(scm);
		h.setDate("Date", System.currentTimeMillis());
		h.set("Server", Server.VERSION);
		if (request.shouldKeepAlive()) {
			h.set("Connection", "Keep-Alive");
			h.set("Keep-Alive", "timeout=5, max=99");
			if (!h.contains("Content-Length")) {
				h.set("Content-Length", 0);
			}
		} else {
			h.set("Connection", "close");
		}
		sb.append(h.toResponse());
		sb.append("\r\n");
		headers = sb.toString().getBytes(CharSet.UTF8);
	}

	/**
	 * Convert a status code to the nearest HTTP 10 status code.
	 * 
	 * @param statusCode
	 *            a http 11 status code.
	 * @return a http 10 status code.
	 */
	private int convertToHttp10(int statusCode) {
		switch (statusCode) {
		case 303:
			return 302;
		case 307:
			return 301;
		}
		return statusCode;
	}

	/**
	 * Create a response with a status code but a zero content length. If
	 * writing a body do not use this constructor.
	 * 
	 * @param statusCode
	 */
	public HttpResponse(Request request, int statusCode) {
		this.statusCode = statusCode;
		StringBuilder sb = new StringBuilder();
		String scm = STATUS_CODES_11.get(statusCode);
		if (request.isHttp10()) {
			statusCode = convertToHttp10(statusCode);
			scm = STATUS_CODES_10.get(statusCode);

		}
		if (scm == null) {
			scm = "HTTP/1.1 " + statusCode + " UNDEFINED\r\n";
		}
		ResponseHeaders h = new ResponseHeaders();
		h.setDate("Date", System.currentTimeMillis());
		h.set("Server", Server.VERSION);
		if (request.shouldKeepAlive()) {
			h.set("Connection", "Keep-Alive");
			h.set("Keep-Alive", "timeout=5, max=99");
			h.set("Content-Length", 0);
		} else {
			h.set("Connection", "close");
		}
		sb.append(scm);
		sb.append(h.toResponse());
		sb.append("\r\n");
		headers = sb.toString().getBytes(CharSet.UTF8);
	}

	/**
	 * Create a response with a status code only for error reporting.
	 * 
	 * @param statusCode
	 */
	public HttpResponse(int statusCode) {
		this.statusCode = statusCode;
		StringBuilder sb = new StringBuilder();
		String scm = STATUS_CODES_11.get(statusCode);
		ResponseHeaders h = new ResponseHeaders();
		h.setDate("Date", System.currentTimeMillis());
		h.set("Server", Server.VERSION);
		h.set("Connection", "close");
		sb.append(scm);
		sb.append(h.toResponse());
		sb.append("\r\n");
		headers = sb.toString().getBytes(CharSet.UTF8);
	}

	/**
	 * Load status codes form the named file
	 * 
	 * @param codesFile
	 *            a file name in the classpath in the same package as this class
	 *            containing status code: status message mappings.
	 * @param statusCodes11
	 *            a map of status codes.
	 */
	private static void loadCodes(String codesFile,
			Map<Integer, String> statusCodes11) {

		try {
			Properties p = new Properties();
			p.load(HttpResponse.class.getResourceAsStream(codesFile));
			for (Object k : p.keySet()) {
				statusCodes11.put(Integer.parseInt((String) k),
						p.getProperty((String) k) + "\r\n");
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * output the headers to the connection.
	 * 
	 * @see uk.co.tfd.threaded.OutputHandler#output(uk.co.tfd.threaded.RequestHandler)
	 */
	public void output(RequestHandler requestHandler) throws IOException {
		requestHandler.writeByte(headers, 0, headers.length);
	}

	/**
	 * get the committed status code.
	 * 
	 * @see uk.co.tfd.threaded.OutputHandler#getStatusCode()
	 */
	public int getStatusCode() {
		return statusCode;
	}

}
