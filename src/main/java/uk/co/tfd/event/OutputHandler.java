package uk.co.tfd.event;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * This handler is attached to a SelectableChannel that is writable. When it is
 * writable, the fillOutputBuffer method will be called to get bytes to write to
 * the channel.
 * 
 * @author ieb
 * 
 */
public interface OutputHandler extends EventHandler {

	/**
	 * Fill the output buffer with some output, that will then be sent to the
	 * EventSource that generated the ready to write event.
	 * 
	 * @param buffer
	 * @return
	 * @throws IOException 
	 */
	Response fillOutputBuffer(ByteBuffer buffer) throws IOException;

}
