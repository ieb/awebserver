package uk.co.tfd.threaded.methods;

import java.io.IOException;

import uk.co.tfd.HttpRequestException;
import uk.co.tfd.Request;
import uk.co.tfd.ResponseHeaders;
import uk.co.tfd.threaded.OutputHandler;
import uk.co.tfd.threaded.RequestHandler;
import uk.co.tfd.threaded.ThreadedMethod;
import uk.co.tfd.threaded.responses.HttpResponse;

public class OptionsMethod implements ThreadedMethod {

	private Request request;

	public OptionsMethod(Request request) {
		this.request = request;
	}

	public int processRequest(RequestHandler requestHandler, long endKeepAlive)
			throws IOException, HttpRequestException {
		if ( "*".equals(request.getPathInfo()) ) {
			ResponseHeaders h = new ResponseHeaders();
			h.set("Allow", "GET,HEAD,POST");
			OutputHandler o = new HttpResponse(request,200,h);
			o.output(requestHandler);
			return o.getStatusCode();
		} else {
			ResponseHeaders h = new ResponseHeaders();
			h.set("Allow", "GET,HEAD");
			OutputHandler o = new HttpResponse(request,200,h);
			o.output(requestHandler);
			return o.getStatusCode();
		}
	}

}
