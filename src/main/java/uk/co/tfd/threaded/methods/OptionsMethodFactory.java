package uk.co.tfd.threaded.methods;

import uk.co.tfd.HttpRequestProtocolException;
import uk.co.tfd.Method;
import uk.co.tfd.MethodFactory;
import uk.co.tfd.Request;


public class OptionsMethodFactory implements MethodFactory {

	public Method create(Request request) throws HttpRequestProtocolException {
		return new OptionsMethod(request);
	}
}